"use strict";

// Required
const Web3 = require("web3");
const mysqlPayout = require('mysql');
const mysql = require('mysql2');
const request = require ("request");
const getJSON = require('get-json');
const net = require('net');

// Settings & Configs
const botSettings = require("./cfgs/config.json");
const miscSettings = require("./cfgs/settings.json");
// const botChans = require("./cfgs/botchans.json");
const functions = require('./cfgs/functions');

// Payouts Configs
const messageSend = "Enjoy the EGEM, with love from the EGEM bot.";

// Create Main SQL Pool
const con = mysql.createPool({
  connectionLimit : 5,
  host: botSettings.mysqlip,
  user: botSettings.mysqluser,
  password: botSettings.mysqlpass,
  database: botSettings.mysqldb
});

// Create Payout SQL Pool
const conPayout = mysqlPayout.createPool({
  connectionLimit : 5,
  host: botSettings.mysqlip,
  user: botSettings.mysqluser,
  password: botSettings.mysqlpass,
  database: botSettings.mysqldb
});

// EtherGem web3
const web3 = new Web3(new Web3.providers.IpcProvider(miscSettings.web3IPC, net));
// const web3 = new Web3(new Web3.providers.HttpProvider(miscSettings.web3HTTP));

// Autopay users
function cycleAutoPay() {
  conPayout.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
    let sql1 = "SELECT creditsUse, totalCredits, totalMNRewards FROM usersystems";
    conPayout.query(sql1, function (err, data, fields){
      let inUse = data[0]['creditsUse'];
      let totalCredits = Number(data[0]['totalCredits']/Math.pow(10,18)).toFixed(0);
      let totalMNRewards = Number(data[0]['totalMNRewards']/Math.pow(10,18)).toFixed(0);

        console.log(totalCredits);
        console.log(totalMNRewards);

        if (inUse == 1) {
          console.log("In use will try next cycle");
        } else {
          let sql2 = "SELECT address, userId, mnrewards, autopay, paylimit FROM userdata";
          conPayout.query(sql2, function (err, result, fields){
            if (!result) return log("No Results.");
            let obj = JSON.stringify(result);
            let parsed = JSON.parse(obj);
            let userdata = result;
            function delay() {
              return new Promise(resolve => setTimeout(resolve, 30));
            }
            async function delayedLog(item) {
              await delay();
              let address = item.address;
              let userId = item.userId;
              let name = item.userName;
              let mnrewards = Number(item.mnrewards);
              let autopay = item.autopay;
              let paylimit = Number(item.paylimit*Math.pow(10,18));
              let autoFee = 100000000000000;
              let baseBalance = 0;
              if (autopay === "1" && mnrewards >= paylimit && address != "0") {
                let value = (Number(mnrewards) - Number(autoFee));
                let finValue = functions.numberToString(value);
                console.log(value)
                console.log(userId+" | "+address+ " | Paid " + (value/Math.pow(10,18)) + " EGEM | New Bal: " + baseBalance);

                web3.eth.sendTransaction({
                    from: botSettings.addressBot,
                    to: address,
                    gas: web3.utils.toHex(miscSettings.txgas),
                    value: web3.utils.toHex(finValue),
                    data: web3.utils.toHex(messageSend)
                })
                .on('transactionHash', function(hash){
                  conPayout.getConnection(function(err, connection) {
                    if (err) throw err; // not connected!
                    let sql5 = "INSERT INTO txdatasent(`hash`,`to`,`value`) values(?,?,?)";
                    conPayout.query(sql5,[hash,address,finValue]);
                    let sql6 = `UPDATE userdata SET lasttx = ? WHERE address = ?`;
                    conPayout.query(sql6, [hash, address]);
                    let sql7 = `UPDATE userdata SET mnrewards =? WHERE userId = ?`;
                    conPayout.query(sql7, [baseBalance,userId]);
                    console.log("Payment sent: " + hash);
                    conPayout.releaseConnection(connection);
                  });
                })
                .on('error', console.log);
              }
            }
            async function processArray(array) {
              console.log("Cycle auto pay started.")
              let sql3 = `UPDATE usersystems SET creditsUse = "1" WHERE id = "1"`;
              await conPayout.query(sql3);
              for (const item of array) {
                await delayedLog(item);
              }
              let sql4 = `UPDATE usersystems SET creditsUse = "0" WHERE id = "1"`;
              await conPayout.query(sql4);
              console.log("Cycle auto pay complete "+ functions.dateTime())
            }
            processArray(userdata);
          })
        }

    })
    conPayout.releaseConnection(connection);
  })
}

function returnBal(minbal) {
  let minBig = Number(minbal*Math.pow(10,18));
  web3.eth.getBalance(botSettings.addressBot, "latest")
  .then(function(result2){
    var botfunds = Number(result2);
    if (botfunds > minBig) {
      console.log("true/paying: "+ botfunds+"/"+minBig);
      cycleAutoPay();
    } else {
      console.log("false/lowfunds: "+ botfunds+"/"+minBig);
    }
  });
}

function autoPay() {
  con.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
    let sql8 = "SELECT totalCredits,totalMNRewards FROM usersystems";
    con.query(sql8, function (err, data, fields){
      var creds = Number(data[0]['totalCredits']/Math.pow(10,18)).toFixed(0);
      var mnrew = Number(data[0]['totalMNRewards']/Math.pow(10,18)).toFixed(0);
      var totalCredits = Number(Number(creds) + Number(mnrew));
      var run = returnBal(totalCredits);
    })
    con.releaseConnection(connection);
  })
}

const runJob1 = function do1() {
  autoPay();
}

autoPay();

setInterval(runJob1,3600000);
