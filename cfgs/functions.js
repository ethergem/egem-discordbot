const Web3 = require("web3");
const InputDataDecoder = require('ethereum-input-data-decoder');
const mysqlPayout = require('mysql');
const mysql = require('mysql2');
const request = require ("request");
const getJSON = require('get-json');
const net = require('net');

// Settings & Configs
const botSettings = require("./config.json");
const miscSettings = require("./settings.json");

// Contract ABI's
const contractMultiSig = require('./contractABI/multisig');

// EtherGem web3
const web3 = new Web3(new Web3.providers.IpcProvider(miscSettings.web3IPC, net));
// const web3 = new Web3(new Web3.providers.HttpProvider(miscSettings.web3HTTP));

// Multisig Config
const decoder = new InputDataDecoder(contractMultiSig);
const botAddress = botSettings.addressBot;
const contractAddressMultiSig = botSettings.contractMulti;
const walletMultisig = new web3.eth.Contract(contractMultiSig, contractAddressMultiSig);

// Create Main SQL Pool
const con = mysql.createPool({
  connectionLimit : 10,
  host: botSettings.mysqlip,
  user: botSettings.mysqluser,
  password: botSettings.mysqlpass,
  database: botSettings.mysqldb
});

// Create Payout SQL Pool
const conPayout = mysqlPayout.createPool({
  connectionLimit : 5,
  host: botSettings.mysqlip,
  user: botSettings.mysqluser,
  password: botSettings.mysqlpass,
  database: botSettings.mysqldb
});

//
function toFixed(num, fixed) {
    var re = new RegExp('^-?\\d+(?:\.\\d{0,' + (fixed || -1) + '})?');
    return num.toString().match(re)[0];
}

// Number to string work around for bignumber and scientific-notation.
function numberToString(num){
    let numStr = String(num);

    if (Math.abs(num) < 1.0)
    {
        let e = parseInt(num.toString().split('e-')[1]);
        if (e)
        {
            let negative = num < 0;
            if (negative) num *= -1
            num *= Math.pow(10, e - 1);
            numStr = '0.' + (new Array(e)).join('0') + num.toString().substring(2);
            if (negative) numStr = "-" + numStr;
        }
    }
    else
    {
        let e = parseInt(num.toString().split('+')[1]);
        if (e > 20)
        {
            e -= 20;
            num /= Math.pow(10, e);
            numStr = num.toString() + (new Array(e + 1)).join('0');
        }
    }

    return numStr;
}

function dateTime() {
  var today = new Date();
  var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
  var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  var dateTime = date+' '+time;
  return dateTime;
}


// Compare who has left but is in bot still.
function compareLeavers(all,registered) {
  let users = [];
  let i;
  for (i in registered) {
    let receiver = registered[i]['userId'];
    if (!all.includes(receiver) && receiver !== "0") {
      users.push(receiver);
    }
  }
  console.log(users);
  return users
};

// Read and store api data for long term viewing
function logData() {
  con.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
    try {
      con.query("SELECT avgusd,currentSupply,totalNodes,tierOneNodes,tierTwoNodes,totalCredits,totalMNRewards,tierOnePay15,tierTwoPay15 FROM usersystems WHERE id = 1", function (err, result, fields){
        try {
          let price = result[0]['avgusd'];
          let currentSupply = result[0]['currentSupply'];
          let totalNodes = result[0]['totalNodes'];
          let tierOneNodes = result[0]['tierOneNodes'];
          let tierTwoNodes = result[0]['tierTwoNodes'];
          let totalCredits = (result[0]['totalCredits']/Math.pow(10,18));
          let totalMNRewards = (result[0]['totalMNRewards']/Math.pow(10,18));
          let t1pay = (result[0]['tierOnePay15']/Math.pow(10,18));
          let t2pay = (result[0]['tierTwoPay15']/Math.pow(10,18));

          con.query("INSERT INTO daily15(`avgusd`,`currentSupply`,`totalNodes`,`tierOneNodes`,`tierTwoNodes`,`totalCredits`,`totalMNRewards`,`t1pay`,`t2pay`) values(?,?,?,?,?,?,?,?,?)",[price,currentSupply,totalNodes,tierOneNodes,tierTwoNodes,totalCredits,totalMNRewards,t1pay,t2pay]);
          console.log("Stats Data Stored.");
        } catch (e) {
          console.log(e)
        }
      })
    } catch (e) {
      console.log(e)
    }
    con.releaseConnection(connection);
  })
}

// Request funds

function requestFunds(args,message) {
  var amount = args[0];
  var value = numberToString(amount*Math.pow(10,18));

  message.reply(amount);

  function callWallet(value) {
    walletMultisig.methods.submitTransaction(botAddress, web3.utils.numberToHex(value), "0x").send({from: botAddress, gas: '300000'})
    .on('transactionHash', function(hash){
        message.reply(hash);
        con.getConnection(function(err, connection) {
            if (err) throw err; // not connected!
            let functionCall = "submitTransaction("+value+")";
            con.query("INSERT INTO txrequests(`hash`,`to`,`functionCall`) values(?,?,?)",[hash,contractAddressMultiSig,functionCall]);
            con.releaseConnection(connection);
        })
        message.reply("Funds requested please complete signer process to finish.");
    })
    .on('error', console.log);
  }
  callWallet(value);
}

// Spin toggle
function spinToggle(message) {
  let idnum = "1";
  con.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
    con.query("SELECT slotGame FROM usersystems WHERE id = ?", idnum, function (err, result) {
      if (err) return message.reply("No Results.");
      try {
        let parsed = JSON.stringify(result);
        let obj = JSON.parse(parsed);
        let slotGame = obj[0]["slotGame"];

        if (slotGame == "1") {
          let response = "0";
          con.query(`UPDATE usersystems SET slotGame =? WHERE id = ?`, [response,idnum]);
          con.releaseConnection(connection);
          return message.reply("Game now set to online.");
        }
        if (slotGame == "0") {
          let response = "1";
          con.query(`UPDATE usersystems SET slotGame =? WHERE id = ?`, [response,idnum]);
          con.releaseConnection(connection);
          return message.reply("Game now set to offline.");
        }

      }catch(e){
        console.log("ERROR ::",e)
        con.releaseConnection(connection);
        return message.reply("Error");
      }
    })
  })
}

// Roll toggle
function rollToggle(message) {
  let idnum = "1";
  con.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
    con.query("SELECT rollGame FROM usersystems WHERE id = ?", idnum, function (err, result) {
      if (err) return message.reply("No Results.");
      try {
        let parsed = JSON.stringify(result);
        let obj = JSON.parse(parsed);
        let rollGame = obj[0]["rollGame"];

        if (rollGame == "1") {
          let response = "0";
          con.query(`UPDATE usersystems SET rollGame =? WHERE id = ?`, [response,idnum]);
          con.releaseConnection(connection);
          return message.reply("Game now set to online.");
        }
        if (rollGame == "0") {
          let response = "1";
          con.query(`UPDATE usersystems SET rollGame =? WHERE id = ?`, [response,idnum]);
          con.releaseConnection(connection);
          return message.reply("Game now set to offline.");
        }

      }catch(e){
        console.log("ERROR ::",e)
        con.releaseConnection(connection);
        return message.reply("Error");
      }
    })
  })
}

// Reward
function rewardUser(args,message) {
  let sqlGame = "SELECT creditsUse FROM usersystems";
  let sql = "SELECT credits FROM userdata WHERE userId = ?";

  var sender = message.author.id;
  var person = args[0];

  //var userTipped = person.id;
  const removeEnd = person.slice(0, -1);
  var userTipped = removeEnd.replace( /^\D+/g, '');

  var user = userTipped;
  var reward = args[1];
  var extra = args[2];

  var maxAllowed = Number(510*Math.pow(10,18));
  var minAllowed = Number(1000000000000);
  var userReward = Number(reward*Math.pow(10,18));

  if (sender == user) {
    return message.reply("No self rewarding allowed.");
  }

  var ifNeg = Math.sign(reward);
  if (ifNeg == "-1") {
    return message.reply("You can't reward a negative amount.");
  }

  if(isNaN(reward)){
   return message.reply("That is not a valid number, Abuse will result in a disabled account.");
  }

  if (userReward <= minAllowed) {
    return message.reply("Im sorry you can not reward that amount! MIN 0.000001.");
  }

  if (userReward >= maxAllowed) {
    return message.reply("Im sorry you can not reward that amount! MAX 500.");
  }

  if (!userReward || !user) {
    return message.reply("Missing variables to send reward. '/reward discordid value'");
  }

  if (extra != null) {
    return message.reply("No junk please, my owner feeds me enough.")
  }

  con.getConnection(function(err, connection) {
    connection.query(sqlGame, function (err, data, fields){
      var inUse = data[0]['creditsUse'];
      if (inUse == 1) {
        return message.reply("Please try again in 1 minute, background actions being done currently.");
      } else {
          connection.query(sql, user, function (err, result) {
            try {
              let credits = result[0]['credits'];
              console.log(credits);
              console.log(userReward);
              console.log(user);

              var finalXfer = (Number(credits) + Number(userReward));

              console.log(finalXfer);

              connection.query(`UPDATE userdata SET credits = ? WHERE userId = ?`, [numberToString(finalXfer), user]);

              message.reply("You have rewarded a user: "+ reward + " EGEM.")

            } catch (e) {
              console.log(e);
              message.reply("User is prolly not registered with our bot.")
            }
          });
          connection.release();
      }
    })
  })
}

// Ping Nodes

async function queryNodes(){
  con.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
          console.log("**#1 PEER UPDATE STARTED** "+ dateTime());
          function delay() {
            return new Promise(resolve => setTimeout(resolve, 100));
          }
          async function delayedLog(item) {
            await delay();

            var balance = parseInt(item.balance);
            var count = "0";

            if (balance >= 10000) {
              con.query("SELECT n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15,n16,n17,n18,n19,n20 FROM usernodes1 WHERE id=?", item.id, async function (err, result) {
                let parsed = JSON.stringify(result);
                let obj = JSON.parse(parsed);
                con.query(`UPDATE usernodes1 SET count = ? WHERE id = ?`, [count, item.id]);
                let i;
                for (i = 1; i < miscSettings.nodeCount; i++) {
                  try {
                    var ip = obj[0]['n'+i+'']
                    if (ip == 0) {
                      // skip
                    } else {
                      async function pingNode(ip,i) {
                        await request({
                            url: "http://"+ip+":8895",
                            method: "POST",
                            json: true,
                            body: miscSettings.peerCount
                        }, function (error, response, body){
                            if (!error) {
                              try {
                                var peers = web3.utils.hexToNumber(body.result);
                                //console.log("#1 Peers: "+ peers+" | "+ item.id +" | " + ip+" | " + i)
                                con.query("UPDATE `usernodes1` SET `count` = `count` + 1 WHERE `id` = ?", [item.id]);
                                con.query("UPDATE `usernodes1` SET `p"+i+"` = ? WHERE `id` = ?", [peers,item.id]);
                                con.query("UPDATE `usernodes1` SET `f"+i+"` = ? WHERE `id` = ?", [count,item.id]);
                              } catch (e) {
                                console.log(e)
                              }
                            } else {
                              let dateTime = dateTime();
                              con.query("UPDATE `userdata` SET `fails` = `fails` + 1 WHERE `id` = ?", [item.id]);
                              con.query("UPDATE `usernodes1` SET `p"+i+"` = ? WHERE `id` = ?", [count,item.id]);
                              con.query("UPDATE `usernodes1` SET `f"+i+"` = ? WHERE `id` = ?", [dateTime,item.id]);
                              //console.log(error)
                            }
                        });
                      }
                      pingNode(ip,i);
                    }
                  } catch (e) {
                    console.log(e+" FAILED USER: "+item.id)
                  }
                }
              })
            } else {
              let i;
              con.query(`UPDATE usernodes1 SET count = ? WHERE id = ?`, [count, item.id]);
              for (i = 1; i < miscSettings.nodeCount; i++) {
                con.query("UPDATE `usernodes1` SET `n"+i+"` = ? WHERE `id` = ?", [count,item.id]);
                con.query("UPDATE `usernodes1` SET `p"+i+"` = ? WHERE `id` = ?", [count,item.id]);
                con.query("UPDATE `usernodes1` SET `f"+i+"` = ? WHERE `id` = ?", [count,item.id]);
                con.query("UPDATE `usernodes1` SET `v"+i+"` = ? WHERE `id` = ?", [count,item.id]);
                //console.log(i+" IPS CLEARED!");
              }
            }

          }

          async function processArray(array) {
            for (const item of array) {
              await delayedLog(item);
            }
            console.log("**#1 DONE** "+ functions.dateTime());
          }

          try {
            con.query("SELECT id, balance FROM userdata", function (err, result) {
              let parsed = JSON.stringify(result);
              let obj = JSON.parse(parsed);
              processArray(obj);
            })
          } catch (e) {
            console.log(e)
          }
          con.releaseConnection(connection);
  });
};

// Get stats
async function queryStats(){
  con.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
          console.log("**#1 STATS UPDATE STARTED** "+ functions.dateTime());
          function delay() {
            return new Promise(resolve => setTimeout(resolve, 100));
          }
          async function delayedLog(item) {
            await delay();
              //console.log(item.earning)
              con.query("SELECT n1,n2,n3,n4,n5,n6,n7,n8,n9,n10 FROM usernodes1 WHERE id=?", item.id, async function (err, result) {
                let parsed = JSON.stringify(result);
                let obj = JSON.parse(parsed);
                var count = "0";
                var i;
                //connection.query(`UPDATE usernodes1 SET count = ? WHERE id = ?`, [count, item.id]);
                for (i = 1; i < miscSettings.nodeCount; i++) {
                  try {
                    var ip = obj[0]['n'+i+'']
                    if (ip == 0) {
                      return;
                    } else {
                      async function pingNode2(ip,i,id,userid) {
                        await request({
                            url: "http://"+ip+":8895",
                            method: "POST",
                            json: true,
                            body: thebooty2
                        }, function (error, response, body){
                            if (!error) {
                              try {
                                let version = body.result;
                                let num1 = 1;
                                let num0 = 0;
                                if (version.includes(minVersion)) {
                                  console.log(body.result + " | " + id + " | " + userid + " | " + ip )
                                  con.query("UPDATE `userdata` SET `outdated` =? WHERE `id` = ?", [num1,item.id]);
                                } else {
                                  con.query("UPDATE `userdata` SET `outdated` =? WHERE `id` = ?", [num0,item.id]);
                                }
                              } catch (e) {
                                //console.log(e)
                              }
                            } else {
                              try {
                                //console.log("STATS FAIL: | "+ ip +" | "+ item.userId)
                              } catch (e) {
                                //console.log(e)
                              }
                              //console.log(error)
                              return;
                            }
                        });
                      }
                      pingNode2(ip,i,item.id,item.userId);
                    }
                  } catch (e) {
                    //console.log(e+" FAILED USER: "+item.id)
                  }
                }
              })
          }

          async function processArray(array) {
            for (const item of array) {
              await delayedLog(item);
            }
            console.log("**#1 DONE** "+ functions.dateTime());
          }

          try {
            con.query("SELECT id, userId, earning FROM userdata", function (err, result) {
              let parsed = JSON.stringify(result);
              let obj = JSON.parse(parsed);
              processArray(obj);
            })
          } catch (e) {
            console.log(e)
          }
          con.releaseConnection(connection);
  });
};

// Counting

async function countUsersEarning(){
  con.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
      console.log("**EARNING USERS COUNT STARTED** "+ functions.dateTime());
      con.query("SELECT SUM(earning) FROM userdata WHERE earning = 1;",function (err, result) {
        let parsed = JSON.stringify(result)
        let obj = JSON.parse(parsed)
        var usersEarning = obj[0]['SUM(earning)']
        con.query(`UPDATE usersystems SET usersEarning =? WHERE id ='1'`, usersEarning);
      })
      con.releaseConnection(connection);
  });
};

async function countBotCoins(){
  con.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
      con.query("SELECT SUM(balance) FROM userdata;",function (err, result) {
        let parsed = JSON.stringify(result)
        let obj = JSON.parse(parsed)
        var coinsCounted = obj[0]['SUM(balance)'];
        con.query(`UPDATE usersystems SET countedBotCoins =? WHERE id ='1'`, coinsCounted);
      })
      con.releaseConnection(connection);
  });
};

async function countTierOne(){
  con.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
      console.log("**NODES ONE COUNT STARTED** "+ functions.dateTime());
      con.query("SELECT queryUseOne,queryUseTwo FROM usersystems",function (err, result) {
        let queryOne = result[0]['queryUseOne'];
        let queryTwo = result[0]['queryUseTwo'];
        if (queryOne == "1" || queryTwo == "1") {
          return console.log("In use trying later");
        }
        con.query("SELECT SUM(count) FROM usernodes1",function (err, result) {
          let parsed = JSON.stringify(result)
          let obj = JSON.parse(parsed)
          var nodesOnline = obj[0]['SUM(count)']
          con.query(`UPDATE usersystems SET tierOneNodes =? WHERE id ='1'`, nodesOnline);
          console.log(nodesOnline)
        })
      })
      con.releaseConnection(connection);
  });
  return;
};

async function countTierTwo(){
  await countTierOne();
  con.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
      console.log("**NODES TWO COUNT STARTED** "+ functions.dateTime());
      con.query("SELECT queryUseOne,queryUseTwo FROM usersystems",function (err, result) {
        let queryOne = result[0]['queryUseOne'];
        let queryTwo = result[0]['queryUseTwo'];
        if (queryOne == "1" || queryTwo == "1") {
          return console.log("In use trying later");
        }
        con.query("SELECT SUM(count) FROM usernodes2",function (err, result) {
          let parsed = JSON.stringify(result)
          let obj = JSON.parse(parsed)
          var nodesOnline = obj[0]['SUM(count)']
          con.query(`UPDATE usersystems SET tierTwoNodes =? WHERE id ='1'`, nodesOnline);
          console.log(nodesOnline)
        })
      })
      con.releaseConnection(connection);
  });
  return;
};

async function totalNodes(){
  await countTierTwo();
  con.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
        console.log("**TOTAL NODES COUNT STARTED** "+ functions.dateTime());
        con.query("SELECT queryUseOne,queryUseTwo FROM usersystems",function (err, result) {
          let queryOne = result[0]['queryUseOne'];
          let queryTwo = result[0]['queryUseTwo'];
          if (queryOne == "1" || queryTwo == "1") {
            return console.log("In use trying later");
          }
          con.query("SELECT tierOneNodes, tierTwoNodes FROM usersystems",function (err, result) {
            let parsed = JSON.stringify(result)
            let obj = JSON.parse(parsed)
            var tierOneNodes = obj[0]['tierOneNodes']
            var tierTwoNodes = obj[0]['tierTwoNodes']
            var totalNodes = (Number(tierOneNodes) + Number(tierTwoNodes))
            con.query(`UPDATE usersystems SET totalNodes =? WHERE id ='1'`, totalNodes);
            console.log(totalNodes)
          })
        })
        con.releaseConnection(connection);
  });
  return;
};

async function updateBalance() {
  con.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
    con.query("SELECT address,addressCreds FROM userdata", function (err, result, fields){
      if (!result) return console.log("No Results UpBAL.");
      let obj = JSON.stringify(result);
      let parsed = JSON.parse(obj);
      let userdata = result;

      function delay() {
      	return new Promise(resolve => setTimeout(resolve, 50));
      }

      async function delayedLog(item) {
      	// await promise return
      	await delay();
      	// log after delay
        if (item.address == "0") {
          //console.log("Skipped user, no address was assigned.")
        } else {
          var getbal = await web3.eth.getBalance(item.address);
          var finbal = Number(getbal/Math.pow(10,18));
          //console.log(finbal + " | Address: " + item.address);
          con.query(`UPDATE userdata SET balance =? WHERE address = ?`, [finbal,item.address]);
        }

        await delay();

        if (item.addressCreds == "0") {
          //console.log("Skipped user #2, no address was assigned.")
        } else {
          var getbal2 = await web3.eth.getBalance(item.addressCreds);
          var finbal2 = Number(getbal2/Math.pow(10,18));
          //console.log(finbal2 + " | Address #2: " + item.addressCreds);
          con.query(`UPDATE userdata SET balanceTwo =? WHERE addressCreds = ?`, [finbal2,item.addressCreds]);
        }
      }

      async function processArray(array) {
        console.log("Balance Update Started");
      	for (const item of array) {
      		await delayedLog(item);
      	}
      	console.log("Balance Update Done");
      }
      processArray(userdata);
    });
    con.releaseConnection(connection);
});
}

async function calcpay(){
  con.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
          console.log("**PAY CALC STARTED** "+ dateTime());
          con.query("SELECT tierOneNodes, tierTwoNodes, tierOnePay, tierTwoPay FROM usersystems",function (err, result) {
            let parsed = JSON.stringify(result)
            let obj = JSON.parse(parsed)

            var tierOneNodes = obj[0]['tierOneNodes']
            var tierTwoNodes = obj[0]['tierTwoNodes']
            var tierOnePay = obj[0]['tierOnePay']
            var tierTwoPay = obj[0]['tierTwoPay']

            var queryOne = (((tierOnePay / (tierOneNodes - tierTwoNodes)) / 24 / 4)*Math.pow(10,18)).toFixed()
            var queryTwo = (((tierTwoPay / tierTwoNodes) / 24 / 4)*Math.pow(10,18)).toFixed()
            //console.log(functions.numberToString(queryOne))
            //console.log(functions.numberToString(queryTwo))
            con.query(`UPDATE usersystems SET tierOnePay15 =? WHERE id ='1'`, numberToString(queryOne));
            con.query(`UPDATE usersystems SET tierTwoPay15 =? WHERE id ='1'`, numberToString(queryTwo));
          })
          con.releaseConnection(connection);
  });
};

async function sumPayRound(){
  con.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
          console.log("**SUM PAY FOR ROUND STARTED** "+ functions.dateTime());
          con.query("SELECT SUM(userpay) FROM userdata",function (err, result) {
            let parsed = JSON.stringify(result)
            let obj = JSON.parse(parsed)
            let sumPayRound = obj[0]['SUM(userpay)'];
            con.query(`UPDATE usersystems SET roundPay =? WHERE id ='1'`, sumPayRound);
          })
          con.releaseConnection(connection);
  });
};

async function sumCredits(){
  con.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
          console.log("**SUM CREDITS STARTED** "+ functions.dateTime());
          con.query("SELECT SUM(credits) FROM userdata",function (err, result) {
            let parsed = JSON.stringify(result)
            let obj = JSON.parse(parsed)
            let sumCredits = obj[0]['SUM(credits)'];
            con.query(`UPDATE usersystems SET totalCredits =? WHERE id ='1'`, functions.numberToString(sumCredits));
          })
          con.releaseConnection(connection);
  });
};

async function sumMNReward(){
  con.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
          console.log("**SUM MNRewards STARTED** "+ functions.dateTime());
          con.query("SELECT SUM(mnrewards) FROM userdata",function (err, result) {
            let parsed = JSON.stringify(result)
            let obj = JSON.parse(parsed)
            let sumMNRewards = obj[0]['SUM(mnrewards)'];
            con.query(`UPDATE usersystems SET totalMNRewards =? WHERE id ='1'`, functions.numberToString(sumMNRewards));
          })
          con.releaseConnection(connection);
  });
};

async function sumUsers(){
  con.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
          console.log("**SUM USERS STARTED** "+ functions.dateTime());
          con.query("SELECT COUNT(id) FROM userdata",function (err, result) {
            let parsed = JSON.stringify(result)
            let obj = JSON.parse(parsed)
            let sumUsers = obj[0]['COUNT(id)'];
            con.query(`UPDATE usersystems SET totalUsers =? WHERE id ='1'`, sumUsers);
          })
          con.releaseConnection(connection);
  });
};

async function storeChainData(){
  var cBlock = await web3.eth.getBlock("latest");
  //var estSupply = ((cEra * 3) + era0sup + era1sup);
  var setNum = cBlock['number'];

  var id = 1;
  try {
    con.getConnection(function(err, connection) {
      try {
        var getprices = getJSON('https://api.coingecko.com/api/v3/coins/ethergem/', function(error, response){
          var getsup = getJSON('https://blockscout.egem.io/api?module=stats&action=coinsupply', function(error, response2){
            if(!error) {
              try {
                var estSupply = response2;
                var priceusd = response["market_data"]["current_price"]["usd"];
                var pricebtc = functions.numberToString(response["market_data"]["current_price"]["btc"]);
                var pricebtcfinal = Number(pricebtc).toFixed(8);

                var row = 1;
                let avgusd = priceusd;
                var mCap = (estSupply * avgusd)
                console.log("Block,supply,mcap,price data updated.");
                console.log(estSupply)
                con.query(`UPDATE usersystems SET currentBlock = ? WHERE id = ?`, [setNum, id]);
                con.query(`UPDATE usersystems SET currentSupply = ? WHERE id = ?`, [estSupply, id]);
                con.query(`UPDATE usersystems SET currentMcap = ? WHERE id = ?`, [mCap, id]);
                con.query(`UPDATE usersystems SET avgusd = ? WHERE id = ?`, [priceusd, id]);
                con.query(`UPDATE usersystems SET avgbtc = ? WHERE id = ?`, [pricebtcfinal, id]);

                con.releaseConnection(connection);
              } catch (e) {
                console.log(e);
              }
            } else {
              console.log(error);
            }
          })
        })
      } catch (e) {
        console.log(e)
      }
    });
  } catch (e) {
    console.log(e)
  }
};

async function sumWithdrawals(){
  con.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
      con.query("SELECT address FROM userdata", function (err, result, fields){
        if (!result) return console.log("No Results SumWD.");
        let obj = JSON.stringify(result);
        let parsed = JSON.parse(obj);
        let userdata = result;
        // console.log(userdata)

        function delay() {
        	return new Promise(resolve => setTimeout(resolve, 100));
        }

        async function delayedLog(item) {
        	// await promise return
        	await delay();
        	// log after delay
          // console.log(item.address)
          con.query("SELECT D.address, D.numberOfWDAmount, DS.TO, COUNT(DS.HASH) AS 'hascount', SUM(DS.VALUE) AS 'value' FROM userdata D INNER JOIN txdatasent DS ON D.address = DS.to WHERE DS.TO = ?", item.address, function (err, result, fields){
            if (!result) return console.log("No Results GET USER.");

            var address = item.address;
            var totalWD = result[0]['hascount'];
            var totalWDAmount = result[0]['value'];

            if (totalWDAmount == null) {
              //do nothing
              return; //console.log("No Withdrawals: "+address);
            } else {
              if (totalWDAmount >= 1000000000000000000000) {
                con.query(`UPDATE userdata SET numberOfWDAmount = ? WHERE address = ?`, [functions.numberToString(totalWDAmount), address]);
                con.query(`UPDATE userdata SET numberOfWD = ? WHERE address = ?`, [totalWD, address]);
              } else {
                con.query(`UPDATE userdata SET numberOfWDAmount = ? WHERE address = ?`, [totalWDAmount, address]);
                con.query(`UPDATE userdata SET numberOfWD = ? WHERE address = ?`, [totalWD, address]);
              }
            }
          })
        }

        async function processArray(array) {
        	for (const item of array) {
        		await delayedLog(item);
        	}
        }
        processArray(userdata);
      });

        console.info("Withdrawals Counted" + " | " + functions.dateTime())
        con.releaseConnection(connection);
      })
};

// Payouts

// Autopay users
function cycleAutoPay() {
  conPayout.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
    let sql1 = "SELECT creditsUse, totalCredits, totalMNRewards FROM usersystems";
    conPayout.query(sql1, function (err, data, fields){
      let inUse = data[0]['creditsUse'];
      let totalCredits = Number(data[0]['totalCredits']/Math.pow(10,18)).toFixed(0);
      let totalMNRewards = Number(data[0]['totalMNRewards']/Math.pow(10,18)).toFixed(0);

        console.log(totalCredits);
        console.log(totalMNRewards);

        if (inUse == 1) {
          console.log("In use will try next cycle");
        } else {
          let sql2 = "SELECT address, userId, mnrewards, autopay, paylimit FROM userdata";
          conPayout.query(sql2, function (err, result, fields){
            if (!result) return log("No Results.");
            let obj = JSON.stringify(result);
            let parsed = JSON.parse(obj);
            let userdata = result;
            function delay() {
              return new Promise(resolve => setTimeout(resolve, 30));
            }
            async function delayedLog(item) {
              await delay();
              let address = item.address;
              let userId = item.userId;
              let name = item.userName;
              let mnrewards = Number(item.mnrewards);
              let autopay = item.autopay;
              let paylimit = Number(item.paylimit*Math.pow(10,18));
              let autoFee = 100000000000000;
              let baseBalance = 0;
              if (autopay === "1" && mnrewards >= paylimit && address != "0") {
                let value = (Number(mnrewards) - Number(autoFee));
                let finValue = functions.numberToString(value);
                console.log(value)
                console.log(userId+" | "+address+ " | Paid " + (value/Math.pow(10,18)) + " EGEM | New Bal: " + baseBalance);
                let sql7 = `UPDATE userdata SET mnrewards =? WHERE userId = ?`;
                conPayout.query(sql7, [baseBalance,userId]);

                web3.eth.sendTransaction({
                    from: botSettings.addressBot,
                    to: address,
                    gas: web3.utils.toHex(miscSettings.txgas),
                    value: web3.utils.toHex(finValue),
                    data: web3.utils.toHex(messageSend)
                })
                .on('transactionHash', function(hash){
                  conPayout.getConnection(function(err, connection) {
                    if (err) throw err; // not connected!
                    let sql5 = "INSERT INTO txdatasent(`hash`,`to`,`value`) values(?,?,?)";
                    conPayout.query(sql5,[hash,address,finValue]);
                    let sql6 = `UPDATE userdata SET lasttx = ? WHERE address = ?`;
                    conPayout.query(sql6, [hash, address]);
                    console.log("Payment sent: " + hash);
                    conPayout.releaseConnection(connection);
                  });
                })
                .on('error', console.log);
              }
            }
            async function processArray(array) {
              console.log("Cycle auto pay started.")
              let sql3 = `UPDATE usersystems SET creditsUse = "1" WHERE id = "1"`;
              await conPayout.query(sql3);
              for (const item of array) {
                await delayedLog(item);
              }
              let sql4 = `UPDATE usersystems SET creditsUse = "0" WHERE id = "1"`;
              await conPayout.query(sql4);
              console.log("Cycle auto pay complete "+ functions.dateTime())
            }
            processArray(userdata);
          })
        }

    })
    conPayout.releaseConnection(connection);
  })
}

function returnBal(minbal) {
  let minBig = Number(minbal*Math.pow(10,18));
  web3.eth.getBalance(botSettings.addressBot, "latest")
  .then(function(result2){
    var botfunds = Number(result2);
    if (botfunds > minBig) {
      console.log("true/paying: "+ botfunds+"/"+minBig);
      cycleAutoPay();
    } else {
      console.log("false/lowfunds: "+ botfunds+"/"+minBig);
    }
  });
}

function autoPay() {
  con.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
    let sql8 = "SELECT totalCredits,totalMNRewards FROM usersystems";
    con.query(sql8, function (err, data, fields){
      var creds = Number(data[0]['totalCredits']/Math.pow(10,18)).toFixed(0);
      var mnrew = Number(data[0]['totalMNRewards']/Math.pow(10,18)).toFixed(0);
      var totalCredits = Number(Number(creds) + Number(mnrew));
      var run = returnBal(totalCredits);
    })
    con.releaseConnection(connection);
  })
}

// exports the variables and functions above so that other modules can use them
module.exports.numberToString = numberToString;
module.exports.toFixed = toFixed;
module.exports.dateTime = dateTime;

module.exports.autoPay = autoPay;
module.exports.sumWithdrawals = sumWithdrawals;
module.exports.storeChainData = storeChainData;
module.exports.sumUsers = sumUsers;
module.exports.sumMNReward = sumMNReward;
module.exports.sumCredits = sumCredits;
module.exports.sumPayRound = sumPayRound;
module.exports.calcpay = calcpay;
module.exports.updateBalance = updateBalance;
module.exports.totalNodes = totalNodes;
module.exports.countTierOne = countTierOne;
module.exports.countTierTwo = countTierTwo;
module.exports.countBotCoins = countBotCoins;
module.exports.countUsersEarning = countUsersEarning;
module.exports.queryNodes = queryNodes;
module.exports.queryStats = queryStats;
module.exports.logData = logData;
module.exports.requestFunds = requestFunds;
module.exports.spinToggle = spinToggle;
module.exports.rollToggle = rollToggle;
module.exports.rewardUser = rewardUser;
