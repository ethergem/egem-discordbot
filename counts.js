// Required
const Web3 = require("web3");
const Discord = require("discord.js");
const mysqlPayout = require('mysql');
const mysql = require('mysql2');
const request = require ("request");
const getJSON = require('get-json');
const net = require('net');

// Configs
const botSettings = require('./cfgs/config.json');
const miscSettings = require('./cfgs/settings.json');
const functions = require('./cfgs/functions.js');
const deadversions = require('./cfgs/deadversions.json');

// Ping Config
const peerCount = {"jsonrpc":"2.0","method":"net_peerCount","params":[],"id":1};
const clientVersion = {"jsonrpc":"2.0","method":"web3_clientVersion","params":[],"id":1};
const nodeCount = 21; // 10 +1 fake

// Create Main SQL Pool
const con = mysql.createPool({
  connectionLimit : 5,
  host: botSettings.mysqlip,
  user: botSettings.mysqluser,
  password: botSettings.mysqlpass,
  database: botSettings.mysqldb
});

// EtherGem web3
const web3 = new Web3(new Web3.providers.IpcProvider(miscSettings.web3IPC, net));
// const web3 = new Web3(new Web3.providers.HttpProvider(miscSettings.web3HTTP));

// Read and store api data for long term viewing

function logData() {
  con.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
    try {
      con.query("SELECT avgusd,currentSupply,totalNodes,tierOneNodes,tierTwoNodes,totalCredits,totalMNRewards,tierOnePay15,tierTwoPay15 FROM usersystems WHERE id = 1", function (err, result, fields){
        try {
          let price = result[0]['avgusd'];
          let currentSupply = result[0]['currentSupply'];
          let totalNodes = result[0]['totalNodes'];
          let tierOneNodes = result[0]['tierOneNodes'];
          let tierTwoNodes = result[0]['tierTwoNodes'];
          let totalCredits = (result[0]['totalCredits']/Math.pow(10,18));
          let totalMNRewards = (result[0]['totalMNRewards']/Math.pow(10,18));
          let t1pay = (result[0]['tierOnePay15']/Math.pow(10,18));
          let t2pay = (result[0]['tierTwoPay15']/Math.pow(10,18));

          con.query("INSERT INTO daily15(`avgusd`,`currentSupply`,`totalNodes`,`tierOneNodes`,`tierTwoNodes`,`totalCredits`,`totalMNRewards`,`t1pay`,`t2pay`) values(?,?,?,?,?,?,?,?,?)",[price,currentSupply,totalNodes,tierOneNodes,tierTwoNodes,totalCredits,totalMNRewards,t1pay,t2pay]);
          console.log("Stats Data Stored.");
        } catch (e) {
          console.log(e)
        }
      })
    } catch (e) {
      console.log(e)
    }
    con.releaseConnection(connection);
  })
}

// Ping Nodes

async function queryNodes(){
  con.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
          console.log("**#1 PEER UPDATE STARTED** "+ functions.dateTime());
          function delay() {
            return new Promise(resolve => setTimeout(resolve, 100));
          }
          async function delayedLog(item) {
            await delay();

            var balance = parseInt(item.balance);
            var count = "0";

            if (balance >= miscSettings.collateral) {
              con.query("SELECT n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15,n16,n17,n18,n19,n20 FROM usernodes1 WHERE id=?", item.id, async function (err, result) {
                let parsed = JSON.stringify(result);
                let obj = JSON.parse(parsed);
                con.query(`UPDATE usernodes1 SET count = ? WHERE id = ?`, [count, item.id]);
                let i;
                for (i = 1; i < nodeCount; i++) {
                  try {
                    var ip = obj[0]['n'+i+'']
                    if (ip == 0) {
                      // skip
                    } else {
                      async function pingNode(ip,i) {
                        await request({
                            url: "http://"+ip+":8895",
                            method: "POST",
                            json: true,
                            body: peerCount
                        }, function (error, response, body){
                            if (!error) {
                              try {
                                var peers = web3.utils.hexToNumber(body.result);
                                //console.log("#1 Peers: "+ peers+" | "+ item.id +" | " + ip+" | " + i)
                                con.query("UPDATE `usernodes1` SET `count` = `count` + 1 WHERE `id` = ?", [item.id]);
                                con.query("UPDATE `usernodes1` SET `p"+i+"` = ? WHERE `id` = ?", [peers,item.id]);
                                con.query("UPDATE `usernodes1` SET `f"+i+"` = ? WHERE `id` = ?", [count,item.id]);
                              } catch (e) {
                                console.log(e)
                              }
                            } else {
                              let dateTime = functions.dateTime();
                              con.query("UPDATE `userdata` SET `fails` = `fails` + 1 WHERE `id` = ?", [item.id]);
                              con.query("UPDATE `usernodes1` SET `p"+i+"` = ? WHERE `id` = ?", [count,item.id]);
                              con.query("UPDATE `usernodes1` SET `f"+i+"` = ? WHERE `id` = ?", [dateTime,item.id]);
                              //console.log(error)
                            }
                        });
                      }
                      pingNode(ip,i);
                    }
                  } catch (e) {
                    console.log(e+" FAILED USER: "+item.id)
                  }
                }
              })
            } else {
              let i;
              con.query(`UPDATE usernodes1 SET count = ? WHERE id = ?`, [count, item.id]);
              for (i = 1; i < nodeCount; i++) {
                con.query("UPDATE `usernodes1` SET `n"+i+"` = ? WHERE `id` = ?", [count,item.id]);
                con.query("UPDATE `usernodes1` SET `p"+i+"` = ? WHERE `id` = ?", [count,item.id]);
                con.query("UPDATE `usernodes1` SET `f"+i+"` = ? WHERE `id` = ?", [count,item.id]);
                con.query("UPDATE `usernodes1` SET `v"+i+"` = ? WHERE `id` = ?", [count,item.id]);
                //console.log(i+" IPS CLEARED!");
              }
            }

          }

          async function processArray(array) {
            for (const item of array) {
              await delayedLog(item);
            }
            console.log("**#1 DONE** "+ functions.dateTime());
          }

          try {
            con.query("SELECT id, balance FROM userdata", function (err, result) {
              let parsed = JSON.stringify(result);
              let obj = JSON.parse(parsed);
              processArray(obj);
            })
          } catch (e) {
            console.log(e)
          }
          con.releaseConnection(connection);
  });
};

// Get version stats

async function queryStats1(){
  con.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
          console.log("**#1 STATS UPDATE STARTED** "+ functions.dateTime());
          function delay() {
            return new Promise(resolve => setTimeout(resolve, 50));
          }
          async function delayedLog(item) {
            await delay();
              //console.log(item.earning)
              con.query("SELECT n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15,n16,n17,n18,n19,n20 FROM usernodes1 WHERE id=?", item.id, async function (err, result) {
                let parsed = JSON.stringify(result);
                let obj = JSON.parse(parsed);
                var count = "0";
                var i;
                //connection.query(`UPDATE usernodes1 SET count = ? WHERE id = ?`, [count, item.id]);
                for (i = 1; i < nodeCount; i++) {
                  try {
                    var ip = obj[0]['n'+i+'']
                    if (ip == 0) {
                      return;
                    } else {
                      async function pingNode2(ip,i,id,userid) {
                        request({
                          url: "http://" + ip + ":8895",
                          method: "POST",
                          json: true,
                          body: clientVersion
                        }, function (error, response, body) {
                          if (!error) {
                            try {
                              let version = body.result;

                              if (deadversions.includes(version, 0)) {
                                //console.log(`Bad Node: ${version} | ${ip} | ${userid} | ${id}`);
                                con.query("UPDATE `usernodes1` SET `n" + i + "` = ? WHERE `id` = ?", [count, item.id]);
                                con.query("UPDATE `usernodes1` SET `p" + i + "` = ? WHERE `id` = ?", [count, item.id]);
                                con.query("UPDATE `usernodes1` SET `f" + i + "` = ? WHERE `id` = ?", [count, item.id]);
                                con.query("UPDATE `usernodes1` SET `v" + i + "` = ? WHERE `id` = ?", [count, item.id]);
                              } else {
                                //console.log(`Good Node: ${version} | ${ip} | ${userid} | ${id}`);
                                con.query("UPDATE `usernodes1` SET `v" + i + "` = ? WHERE `id` = ?", [version, item.id]);
                              }

                            } catch (e) {
                              console.log(e)
                            }
                          } else {
                            return;
                          }
                        });
                      }
                      pingNode2(ip,i,item.id,item.userId);
                    }
                  } catch (e) {
                    //console.log(e+" FAILED USER: "+item.id)
                  }
                }
              })
          }

          async function processArray(array) {
            for (const item of array) {
              await delayedLog(item);
            }
            console.log("**#1 DONE** "+ functions.dateTime());
          }

          try {
            con.query("SELECT id, userId, earning FROM userdata", function (err, result) {
              let parsed = JSON.stringify(result);
              let obj = JSON.parse(parsed);
              processArray(obj);
            })
          } catch (e) {
            console.log(e)
          }
          con.releaseConnection(connection);
  });
};

// Counting

async function countUsersEarning(){
  con.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
      console.log("**EARNING USERS COUNT STARTED** "+ functions.dateTime());
      con.query("SELECT SUM(earning) FROM userdata WHERE earning = 1;",function (err, result) {
        let parsed = JSON.stringify(result)
        let obj = JSON.parse(parsed)
        var usersEarning = obj[0]['SUM(earning)']
        con.query(`UPDATE usersystems SET usersEarning =? WHERE id ='1'`, usersEarning);
      })
      con.releaseConnection(connection);
  });
};

async function countBotCoins(){
  con.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
      console.log("**BOT COIN COUNT STARTED** "+ functions.dateTime());
      con.query("SELECT SUM(balance) FROM userdata;",function (err, result) {
        let parsed = JSON.stringify(result)
        let obj = JSON.parse(parsed)
        var coinsCounted = obj[0]['SUM(balance)'];
        con.query(`UPDATE usersystems SET countedBotCoins =? WHERE id ='1'`, coinsCounted);
      })
      con.releaseConnection(connection);
  });
};

async function countTierOne(){
  con.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
      console.log("**NODE COUNT STARTED** "+ functions.dateTime());
      con.query("SELECT queryUseOne FROM usersystems",function (err, result) {
        let queryOne = result[0]['queryUseOne'];
        if (queryOne == "1") {
          return console.log("In use trying later");
        }
        con.query("SELECT SUM(count) FROM usernodes1",function (err, result) {
          let parsed = JSON.stringify(result)
          let obj = JSON.parse(parsed)
          var nodesOnline = obj[0]['SUM(count)']
          con.query(`UPDATE usersystems SET tierOneNodes =? WHERE id ='1'`, nodesOnline);
          con.query(`UPDATE usersystems SET totalNodes =? WHERE id ='1'`, nodesOnline);
          console.log(nodesOnline)
        })
      })
      con.releaseConnection(connection);
  });
  return;
};

async function updateBalance() {
  con.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
    con.query("SELECT address,addressCreds FROM userdata", function (err, result, fields){
      if (!result) return console.log("No Results UpBAL.");
      let obj = JSON.stringify(result);
      let parsed = JSON.parse(obj);
      let userdata = result;

      function delay() {
      	return new Promise(resolve => setTimeout(resolve, 50));
      }

      async function delayedLog(item) {
      	// await promise return
      	await delay();
      	// log after delay
        if (item.address == "0") {
          //console.log("Skipped user, no address was assigned.")
        } else {
          var getbal = await web3.eth.getBalance(item.address);
          var finbal = Number(getbal/Math.pow(10,18));
          //console.log(finbal + " | Address: " + item.address);
          con.query(`UPDATE userdata SET balance =? WHERE address = ?`, [finbal,item.address]);
        }

        await delay();

        if (item.addressCreds == "0") {
          //console.log("Skipped user #2, no address was assigned.")
        } else {
          var getbal2 = await web3.eth.getBalance(item.addressCreds);
          var finbal2 = Number(getbal2/Math.pow(10,18));
          //console.log(finbal2 + " | Address #2: " + item.addressCreds);
          con.query(`UPDATE userdata SET balanceTwo =? WHERE addressCreds = ?`, [finbal2,item.addressCreds]);
        }
      }

      async function processArray(array) {
        console.log("Balance Update Started");
      	for (const item of array) {
      		await delayedLog(item);
      	}
      	console.log("Balance Update Done");
      }
      processArray(userdata);
    });
    con.releaseConnection(connection);
});
}

async function calcpay(){
  con.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
          console.log("**PAY CALC STARTED** "+ functions.dateTime());
          con.query("SELECT tierOneNodes, tierOnePay FROM usersystems",function (err, result) {
            let parsed = JSON.stringify(result)
            let obj = JSON.parse(parsed)

            var tierOneNodes = obj[0]['tierOneNodes']
            var tierOnePay = obj[0]['tierOnePay']

            var queryOne = (((tierOnePay / tierOneNodes) / 24 / 4)*Math.pow(10,18)).toFixed()
            con.query(`UPDATE usersystems SET tierOnePay15 =? WHERE id ='1'`, functions.numberToString(queryOne));

          })
          con.releaseConnection(connection);
  });
};

async function sumPayRound(){
  con.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
          console.log("**SUM PAY FOR ROUND STARTED** "+ functions.dateTime());
          con.query("SELECT SUM(userpay) FROM userdata",function (err, result) {
            let parsed = JSON.stringify(result)
            let obj = JSON.parse(parsed)
            let sumPayRound = obj[0]['SUM(userpay)'];
            con.query(`UPDATE usersystems SET roundPay =? WHERE id ='1'`, sumPayRound);
          })
          con.releaseConnection(connection);
  });
};

async function sumCredits(){
  con.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
          console.log("**SUM CREDITS STARTED** "+ functions.dateTime());
          con.query("SELECT SUM(credits) FROM userdata",function (err, result) {
            let parsed = JSON.stringify(result)
            let obj = JSON.parse(parsed)
            let sumCredits = obj[0]['SUM(credits)'];
            con.query(`UPDATE usersystems SET totalCredits =? WHERE id ='1'`, functions.numberToString(sumCredits));
          })
          con.releaseConnection(connection);
  });
};

async function sumMNReward(){
  con.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
          console.log("**SUM MNRewards STARTED** "+ functions.dateTime());
          con.query("SELECT SUM(mnrewards) FROM userdata",function (err, result) {
            let parsed = JSON.stringify(result)
            let obj = JSON.parse(parsed)
            let sumMNRewards = obj[0]['SUM(mnrewards)'];
            con.query(`UPDATE usersystems SET totalMNRewards =? WHERE id ='1'`, functions.numberToString(sumMNRewards));
          })
          con.releaseConnection(connection);
  });
};

async function sumUsers(){
  con.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
          console.log("**SUM USERS STARTED** "+ functions.dateTime());
          con.query("SELECT COUNT(id) FROM userdata",function (err, result) {
            let parsed = JSON.stringify(result)
            let obj = JSON.parse(parsed)
            let sumUsers = obj[0]['COUNT(id)'];
            con.query(`UPDATE usersystems SET totalUsers =? WHERE id ='1'`, sumUsers);
          })
          con.releaseConnection(connection);
  });
};

async function storeChainData(){
  var cBlock = await web3.eth.getBlock("latest");
  //var estSupply = ((cEra * 3) + era0sup + era1sup);
  var setNum = cBlock['number'];

  var id = 1;
  try {
    con.getConnection(function(err, connection) {
      try {
        var getprices = getJSON('https://api.coingecko.com/api/v3/coins/ethergem/', async function(error, response){
          if (error) {
            return;
          }
          var getsup = getJSON('https://blockscout.egem.io/api?module=stats&action=coinsupply', async function(error, response2){
            if(!error) {
              try {
                var estSupply = response2;
                var priceusd = response["market_data"]["current_price"]["usd"];
                var pricebtc = functions.numberToString(response["market_data"]["current_price"]["btc"]);
                var pricebtcfinal = Number(pricebtc).toFixed(8);

                var row = 1;
                let avgusd = priceusd;
                var mCap = (estSupply * avgusd)
                console.log("Block,supply,mcap,price data updated.");
                console.log(estSupply)
                con.query(`UPDATE usersystems SET currentBlock = ? WHERE id = ?`, [setNum, id]);
                con.query(`UPDATE usersystems SET currentSupply = ? WHERE id = ?`, [estSupply, id]);
                con.query(`UPDATE usersystems SET currentMcap = ? WHERE id = ?`, [mCap, id]);
                con.query(`UPDATE usersystems SET avgusd = ? WHERE id = ?`, [priceusd, id]);
                con.query(`UPDATE usersystems SET avgbtc = ? WHERE id = ?`, [pricebtcfinal, id]);

                con.releaseConnection(connection);
              } catch (e) {
                console.log(e);
              }
            } else {
              console.log(error);
            }
          })
        })
      } catch (e) {
        console.log(e)
      }
    });
  } catch (e) {
    console.log(e)
  }
};

async function sumWithdrawals(){
  con.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
      con.query("SELECT address FROM userdata", function (err, result, fields){
        if (!result) return console.log("No Results SumWD.");
        let obj = JSON.stringify(result);
        let parsed = JSON.parse(obj);
        let userdata = result;
        // console.log(userdata)

        function delay() {
        	return new Promise(resolve => setTimeout(resolve, 100));
        }

        async function delayedLog(item) {
        	// await promise return
        	await delay();
        	// log after delay
          // console.log(item.address)
          con.query("SELECT D.address, D.numberOfWDAmount, DS.TO, COUNT(DS.HASH) AS 'hascount', SUM(DS.VALUE) AS 'value' FROM userdata D INNER JOIN txdatasent DS ON D.address = DS.to WHERE DS.TO = ?", item.address, function (err, result, fields){
            if (!result) return console.log("No Results GET USER.");

            var address = item.address;
            var totalWD = result[0]['hascount'];
            var totalWDAmount = result[0]['value'];

            if (totalWDAmount == null) {
              //do nothing
              return; //console.log("No Withdrawals: "+address);
            } else {
              if (totalWDAmount >= 1000000000000000000000) {
                con.query(`UPDATE userdata SET numberOfWDAmount = ? WHERE address = ?`, [functions.numberToString(totalWDAmount), address]);
                con.query(`UPDATE userdata SET numberOfWD = ? WHERE address = ?`, [totalWD, address]);
              } else {
                con.query(`UPDATE userdata SET numberOfWDAmount = ? WHERE address = ?`, [totalWDAmount, address]);
                con.query(`UPDATE userdata SET numberOfWD = ? WHERE address = ?`, [totalWD, address]);
              }
            }
          })
        }

        async function processArray(array) {
        	for (const item of array) {
        		await delayedLog(item);
        	}
        }
        processArray(userdata);
      });

        console.info("Withdrawals Counted" + " | " + functions.dateTime())
        con.releaseConnection(connection);
      })
};

// Run Routines

// const presenceReset = function do1(){
// 	rPresence();
// };
// const presence1 = function do2(){
// 	sharePriceUSD();
// };
// const presence2 = function do3(){
// 	shareNodeCount();
// };
// const presence3 = function do4(){
// 	shareAccountCount();
// };
//calcpay();
const pingnodes1 = function do5() {
  queryNodes();
}
const pingnodes2 = function do6() {
  queryStats1();
}
const runCounts1 = function do7() {
  countTierOne();
}
const runCounts2 = function do8() {
  updateBalance();
}
const runCounts3 = function do9() {
  sumWithdrawals();
}
const runCounts4 = function do10() {
  storeChainData();
}

const runCounts5 = function do11() {
  calcpay();
  sumPayRound();
  sumCredits();
  sumMNReward();
  countUsersEarning();
  countBotCoins();
  sumUsers();
}

const logDataTask = function do12() {
  logData();
}

// Interval Config

setInterval(runCounts1,240000);
setInterval(runCounts2,405000);
setInterval(runCounts3,1800000);
setInterval(runCounts4,900000);
setInterval(runCounts5,505000);

setInterval(pingnodes1,330000);
setInterval(pingnodes2,3600000);

// setInterval(presenceReset,15000);
// setInterval(presence1,60000);
// setInterval(presence2,65000);
// setInterval(presence3,70000);

setInterval(logDataTask,900000);
//setInterval(motd,10800000);
//setInterval(notifyFails,1800000);

console.log("Counts and stats started.");
