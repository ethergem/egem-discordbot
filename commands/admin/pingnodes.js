module.exports = {
  name: 'pingnodes',
  description: "",
  async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, mysql2, ipRegex, request, functions) {

    if (!admins.includes(message.author.id)) {
      return message.reply("Not authorized.")
    }

    functions.queryNodes();
    message.reply("Node ping manually started.");

  }
}
