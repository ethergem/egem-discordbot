module.exports = {
  name: 'motd',
  description: "",
  async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, mysql2, ipRegex, request, functions, mysql) {

    if (!admins.includes(message.author.id)) {
      return message.reply("Not authorized.")
    }

    var con = mysql.createConnection({
      host: botSettings.mysqlip,
      user: botSettings.mysqluser,
      password: botSettings.mysqlpass,
      database: botSettings.mysqldb
    });

    con.connect(function(err) {
      if (err) {
        console.error('error connecting: ' + err.stack);
        return;
      }
      console.log('connected as id ' + con.threadId);
    });

    var editedMOTD = args.slice(0).join(" ");
    con.query(`UPDATE usersystems SET motd =? WHERE id = 1`, editedMOTD);
    message.reply("Message of the day has been saved!");

    con.end(function(err) {
      console.log('disconnected as id ' + con.threadId);
    });

  }
}
