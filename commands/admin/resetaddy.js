module.exports = {
  name: 'resetaddy',
  description: "Reset a users node address to empty.",
  async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, mysql2, ipRegex, request, functions, mysql) {

    var con = mysql.createConnection({
      host: botSettings.mysqlip,
      user: botSettings.mysqluser,
      password: botSettings.mysqlpass,
      database: botSettings.mysqldb
    });

    if (!admins.includes(message.author.id)) {
      return message.reply("Not authorized.")
    }

    con.connect(function(err) {
      if (err) {
        console.error('error connecting: ' + err.stack);
        return;
      }
      console.log('connected as id ' + con.threadId);
    });

    var address = "0";
    var person = message.mentions.users.first();
    var userEdited = person.id;

    con.query(`UPDATE userdata SET address = ? WHERE userId = ?`, [address, userEdited]);
    message.reply("User address reset.");

    con.end(function(err) {
      console.log('disconnected as id ' + con.threadId);
    });

  }
}
