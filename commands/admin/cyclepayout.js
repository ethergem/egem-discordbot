module.exports = {
  name: 'cyclepayout',
  description: "",
  async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, mysql2, ipRegex, request, functions) {

    if (!admins.includes(message.author.id)) {
      return message.reply("Not authorized.")
    }

    functions.autoPay();
    message.reply("Node payments manually started.");

  }
}
