const { egemspin,okcolor,warningcolor,footerBranding,img32x32,img32shard,ghlink,web3IPC,web3HTTP,txgas,messageSend } = require('../../cfgs/settings.json');

module.exports = {
  name: 'rainall',
  description: "Rain some egem to everyone online.",
  async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, mysql2, ipRegex, request, functions, mysql) {

    var con = mysql.createConnection({
      host: botSettings.mysqlip,
      user: botSettings.mysqluser,
      password: botSettings.mysqlpass,
      database: botSettings.mysqldb
    });

    con.connect(function(err) {
      if (err) {
        console.error('error connecting: ' + err.stack);
        return;
      }
      console.log('connected as id ' + con.threadId);
    });

    if (!admins.includes(message.author.id)) {
      return message.reply("Not authorized.")
    }

    function addRain(user,amount,balance) {
      let credits = (Number(balance) + Number(amount));
      console.log(user+" | "+amount+" | "+balance+" | "+functions.numberToString(credits));
      let author = findUser(user);
      //author.send("Hi, you are a lucky human i have added "+amount/Math.pow(10,18)+" to your EGEM credits due to a tip/rain.");
      //con.query(`UPDATE userdata SET credits =? WHERE userId =?`, [functions.numberToString(credits),user]);

    }

    // Find User
    function findUser(id) {
      return client.users.cache.find(role => role.id === id);
    }

    // Get all online users.
    function getOnlineUsers(){
      let onlinearray = [];
      const guild = client.guilds.cache.get('818440388381245471');
      guild.members.cache.forEach(member => {
        //var presenceCache = member.guild.presences.cache
        //console.log(member.user.id);
        onlinearray.push(member.user.id);
      })
      //console.log(onlinearray);
      return onlinearray;
    }
    //getOnlineUsers();

    // Compare and return list
    function compareUsers(all,registered) {
      let users = [];
      let i;
      for (i in registered) {
        let receiver = registered[i]['userId'];
        if (all.includes(receiver)) {
          users.push(receiver);
        }
      }
      return users
    };

    let sql1 = "SELECT creditsUse FROM usersystems";
    con.query(sql1, function (err, data, fields){
      var inUse = data[0]['creditsUse'];
      if (inUse == 1) {
        return message.reply("Please try again in 1 minute, background actions being done currently.");
      } else {
        con.query('SELECT userId FROM userdata WHERE addressCreds != "0"', function (err, result){
          if (!result) return console.log("No Results.");
          let parsed = JSON.stringify(result);
          let registered = JSON.parse(parsed);
          let online = getOnlineUsers();
          let compareList = compareUsers(online, registered);
          let compareCount = compareList.length;
          let rainAmount = args[0];
          let rainSend = Number(rainAmount / compareCount);
          let rainWei = rainSend*Math.pow(10,18);

          var ifNeg = Math.sign(rainAmount);
          if (ifNeg == "-1" || isNaN(rainAmount) || rainAmount == null || rainAmount > 1000 || rainAmount < 1) {
            return message.reply("Bad monkey.");
          }

          message.reply("There are "+compareCount+" registered and online users about to get rained "+rainSend+" EGEM each.");

          function delay() {
            return new Promise(resolve => setTimeout(resolve, 150));
          }

          async function delayedLog(item) {
            // await promise return
            await delay();
            con.query("SELECT credits,address FROM userdata WHERE userid =?", item, function (err, result){
              if (!result) return console.log("No Results.");
              let parsed = JSON.stringify(result);
              let obj = JSON.parse(parsed);
              let creds = obj[0]['credits'];
              let address = obj[0]['credits'];
              addRain(item,rainWei,creds);
            })
          }

          async function processArray(array) {
            for (const item of array) {
              await delayedLog(item);
            }
            con.end(function(err) {
              console.log('disconnected as id ' + con.threadId);
            });
          }
          processArray(compareList);

        })
      }
    })

  }
}
