// .
module.exports = {
  name: 'mynodes',
  description: "List of nodes registered within system.",
  async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, mysql2, ipRegex, request, functions, mysql) {

    var con = mysql.createConnection({
      host: botSettings.mysqlip,
      user: botSettings.mysqluser,
      password: botSettings.mysqlpass,
      database: botSettings.mysqldb
    });

    if(message.channel.type === "DM") {
      try {
        let userId = message.author.id;

        con.connect(function(err) {
          if (err) {
            console.error('error connecting: ' + err.stack);
            return;
          }
          console.log('connected as id ' + con.threadId);
        });

        con.query("SELECT id FROM userdata WHERE userId = ?", userId, function (err, result1, fields){
          try {
            let id = result1[0]['id'];
            if (id == 'undefined' || id == null) {
              return message.reply("Not registered!")
            }
            if (err) throw err; // not connected!
            con.query("SELECT * FROM usernodes1 WHERE id = ?", id, function (err, result2, fields){
              if (err) throw err; // not connected!
              var count1 = result2[0]['count'];
              let t1n1 = result2[0]['n1'];
              let t1n2 = result2[0]['n2'];
              let t1n3 = result2[0]['n3'];
              let t1n4 = result2[0]['n4'];
              let t1n5 = result2[0]['n5'];
              let t1n6 = result2[0]['n6'];
              let t1n7 = result2[0]['n7'];
              let t1n8 = result2[0]['n8'];
              let t1n9 = result2[0]['n9'];
              let t1n10 = result2[0]['n10'];
              let t1n11 = result2[0]['n11'];
              let t1n12 = result2[0]['n12'];
              let t1n13 = result2[0]['n13'];
              let t1n14 = result2[0]['n14'];
              let t1n15 = result2[0]['n15'];
              let t1n16 = result2[0]['n16'];
              let t1n17 = result2[0]['n17'];
              let t1n18 = result2[0]['n18'];
              let t1n19 = result2[0]['n19'];
              let t1n20 = result2[0]['n20'];

              let t1p1 = result2[0]['p1'];
              let t1p2 = result2[0]['p2'];
              let t1p3 = result2[0]['p3'];
              let t1p4 = result2[0]['p4'];
              let t1p5 = result2[0]['p5'];
              let t1p6 = result2[0]['p6'];
              let t1p7 = result2[0]['p7'];
              let t1p8 = result2[0]['p8'];
              let t1p9 = result2[0]['p9'];
              let t1p10 = result2[0]['p10'];
              let t1p11 = result2[0]['p11'];
              let t1p12 = result2[0]['p12'];
              let t1p13 = result2[0]['p13'];
              let t1p14 = result2[0]['p14'];
              let t1p15 = result2[0]['p15'];
              let t1p16 = result2[0]['p16'];
              let t1p17 = result2[0]['p17'];
              let t1p18 = result2[0]['p18'];
              let t1p19 = result2[0]['p19'];
              let t1p20 = result2[0]['p20'];

              let t1f1 = result2[0]['f1'];
              let t1f2 = result2[0]['f2'];
              let t1f3 = result2[0]['f3'];
              let t1f4 = result2[0]['f4'];
              let t1f5 = result2[0]['f5'];
              let t1f6 = result2[0]['f6'];
              let t1f7 = result2[0]['f7'];
              let t1f8 = result2[0]['f8'];
              let t1f9 = result2[0]['f9'];
              let t1f10 = result2[0]['f10'];
              let t1f11 = result2[0]['f11'];
              let t1f12 = result2[0]['f12'];
              let t1f13 = result2[0]['f13'];
              let t1f14 = result2[0]['f14'];
              let t1f15 = result2[0]['f15'];
              let t1f16 = result2[0]['f16'];
              let t1f17 = result2[0]['f17'];
              let t1f18 = result2[0]['f18'];
              let t1f19 = result2[0]['f19'];
              let t1f20 = result2[0]['f20'];

              console.log(count1);

              const embed = new Discord.MessageEmbed()
                .setTitle("Result:")
                .setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })

                .setColor(miscSettings.okcolor)
                .setDescription("My Nodes:")
                .setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })
                .setThumbnail(miscSettings.img32shard)

                .setTimestamp()
                .setURL(miscSettings.ghlink)
                .addField("Node Count:",Number(count1)+" Online.")
                .addField("Node 1: "+t1n1 , t1p1+" / PEERS")
                .addField("Node 2: "+t1n2 , t1p2+" / PEERS")
                .addField("Node 3: "+t1n3 , t1p3+" / PEERS")
                .addField("Node 4: "+t1n4 , t1p4+" / PEERS")
                .addField("Node 5: "+t1n5 , t1p5+" / PEERS")
                .addField("Node 6: "+t1n6 , t1p6+" / PEERS")
                .addField("Node 7: "+t1n7 , t1p7+" / PEERS")
                .addField("Node 8: "+t1n8 , t1p8+" / PEERS")
                .addField("Node 9: "+t1n9 , t1p9+" / PEERS")
                .addField("Node 10: "+t1n10 , t1p10+" / PEERS")
                .addField("Node 11: "+t1n11 , t1p11+" / PEERS")
                .addField("Node 12: "+t1n12 , t1p12+" / PEERS")
                .addField("Node 13: "+t1n13 , t1p13+" / PEERS")
                .addField("Node 14: "+t1n14 , t1p14+" / PEERS")
                .addField("Node 15: "+t1n15 , t1p15+" / PEERS")
                .addField("Node 16: "+t1n16 , t1p16+" / PEERS")
                .addField("Node 17: "+t1n17 , t1p17+" / PEERS")
                .addField("Node 18: "+t1n18 , t1p18+" / PEERS")
                .addField("Node 19: "+t1n19 , t1p19+" / PEERS")
                .addField("Node 20: "+t1n20 , t1p20+" / PEERS")

              con.end(function(err) {console.log('disconnected as id ' + con.threadId);});

              return message.reply({ embeds: [embed] })

            })
          } catch (e) {
            console.log(e)
            message.reply("User Not registered, use /register.");
          }
        })


      } catch (e) {
        console.log(e);
      }
    } else {
      return message.reply("Private Message Only!");
    }

  }
}
