module.exports = {
  name: 'register',
  description: "",
  async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, mysql2, ipRegex, request, functions) {

    const con = mysql2.createPool({
      connectionLimit : 25,
      host: botSettings.mysqlip,
      user: botSettings.mysqluser,
      password: botSettings.mysqlpass,
      database: botSettings.mysqldb
    });

    if(message.channel.type === "DM") {
      var userId = message.author.id;
      con.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
        try {
          con.query("SELECT userId, id FROM userdata WHERE userId = ?", userId, function (err, result, fields){
            console.log(result);
            try {
              let userCheck = result[0]['userId'];
              if (userCheck == userId) {
                return message.reply("Already registered.")
              }
            } catch (e) {
              console.log(e)
            }
            let count = "0";
            con.query('INSERT INTO userdata (userId) VALUES (?)', [userId]);
            con.query('INSERT INTO usernodes1 (count) VALUES (?)', [count]);
            
            message.reply("Thank you for registering, please check out wiki for a list of commands... https://wiki.egem.io/en/botcmds.")
          })
        } catch (e) {
          console.log(e)
        }
        con.releaseConnection(connection);
      })
    } else {
      return message.reply("Private Message Only!");
    }

  }
}
