//
module.exports = {
  name: 'addnode',
  description: "Add a node to a slot.",
  async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, mysql2, ipRegex, request, functions, mysql, web3R, userRoles, Duration, blacklist) {

    const con = mysql2.createPool({
      connectionLimit : 20,
      host: botSettings.mysqlip,
      user: botSettings.mysqluser,
      password: botSettings.mysqlpass,
      database: botSettings.mysqldb
    });

    const nodeNums = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20'];
    const tierNums = ['1'];
    const userId = message.author.id;
    const tier = args[0];
    const number = args[1];
    const ip = args[2];

    if(message.channel.type === "DM") {
      con.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
        try {
          //console.log(tier)
          //console.log(number)

          if (tier == null || number == null || ip == null) {
            return message.reply("Fill in all fields: /an tier number ip");
          }
          if (!tierNums.includes(tier)) {
            return message.reply("Not a valid choice, tier must be a number 1.");
          }
          if (!nodeNums.includes(number)) {
            return message.reply("Not a valid choice, number must be a from 1-20.");
          }
          let ipCheck = ipRegex({exact: true}).test(ip);
          if (ipCheck != true) {
            return message.reply("Not a valid choice, must be a valid ip.");
          }
          if (ip.length > 15) {
            return message.reply("Ip addres is to long which prolly means trying to use IPv6 which is not supported.");
          }
          con.query("SELECT id FROM userdata WHERE userId = ?", userId, function (err, result, fields){
            try {
              let myId = result[0]['id']
              con.query("SELECT * FROM usernodes1", function (err, result2, fields){
                if (!err) {
                  let parsed1 = JSON.stringify(result2);

                  //console.log(obj)
                  let theip = '"'+ip+'"';
                  //console.log(theip)
                  var res1 = parsed1.match(theip);

                  if (res1) {
                    message.reply("IP might be taken, try again or message a EGEM admin that there might be a problem.");
                  } else {
                    message.reply("IP Registered, it may take upto 5 minutes for system to check it.");
                    con.query(`UPDATE usernodes`+tier+` SET n`+number+` = ? WHERE id = ?`, [ip, myId]);
                  }
                  //console.log(result2)
                } else {
                  console.log(err)
                }
              })
            } catch (e) {
              console.log(e)
              message.reply("User Not registered, use /register.");
            }
          })
        } catch (e) {
          console.log(e)
        }
        con.releaseConnection(connection);
      })
    } else {
      return message.reply("Private Message Only!");
    }

  }
}
