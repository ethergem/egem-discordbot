module.exports = {
  name: 'wd',
  description: "",
  async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, mysql2, ipRegex, request, functions) {

    var con = mysql2.createPool({
      connectionLimit : 20,
      host: botSettings.mysqlip,
      user: botSettings.mysqluser,
      password: botSettings.mysqlpass,
      database: botSettings.mysqldb
    });

    if(message.channel.type === "DM") {

      var messageSend = "Enjoy the EGEM, with love from the EGEM bot.";
      var userId = message.author.id;
      var wdammount = args[0];
      var extra = args[1];
      var zero = 0;

      if (extra != null) {
        return message.reply("No junk please, my owner feeds me enough.")
      }

      con.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
            let sql = "SELECT creditsUse, totalCredits FROM usersystems";
            con.query(sql, function (err, data){
              let inUse = data[0]['creditsUse'];
              let totalCredits = Number(data[0]['totalCredits']);
              web3.eth.getBalance(botSettings.addressBot, "latest")
              .then(function(balanceResult){
                var botfunds = Number(balanceResult);

                if (inUse == 1) {
                  return message.reply("Payment system in use try again later.");
                } else {
                  if (totalCredits > botfunds) {
                    return message.reply("Funds are needed in system, please try later.");
                  } else {
                    if (wdammount) {
                      // wd ammount
                      let sql = "SELECT credits,addressCreds FROM userdata WHERE userId = ?";
                      con.query(sql, userId, function (err, result){
                        try {
                          let credits = (result[0]['credits']/Math.pow(10,18)).toFixed(0);
                          let creditsBig = result[0]['credits'];
                          let creditsParsed = Number(creditsBig);
                          let wdammountParsed = Number(wdammount*Math.pow(10,18));

                          let address = result[0]['addressCreds'];
                          if (address == 0) {
                            return message.reply("No EGEM address to send payment to.")
                          }

                          let ifNeg = Math.sign(wdammount);

                          if (creditsParsed <= 0 || creditsParsed < wdammountParsed || ifNeg == "-1" || isNaN(wdammountParsed)) {
                            return message.reply("Payment failed, not enough to withdrawal, or error in request.");
                          } else {
                            let value = functions.numberToString(wdammount*Math.pow(10,18));
                            let finalCreds = functions.numberToString(creditsBig - value);

                            let numVal = Number(value);
                            let numCred = Number(creditsBig);
                            let finValue = functions.numberToString(value);

                            if (numVal > numCred) {
                              return message.reply("Payment failed, requested funds greater than balance.");
                            } else {
                              message.reply("Payment sending.");

                              web3.eth.sendTransaction({
                                  from: botSettings.addressBot,
                                  to: address,
                                  gas: web3.utils.toHex(miscSettings.txgas),
                                  value: web3.utils.toHex(finValue),
                                  data: web3.utils.toHex(messageSend)
                              })
                              .on('transactionHash', function(hash){
                                con.getConnection(function(err, connection) {
                                  if (err) throw err; // not connected!
                                  let sql1 = "INSERT INTO txdatasent(`hash`,`to`,`value`) values(?,?,?)";
                                  con.query(sql1,[hash,address,value]);
                                  let sql2 = `UPDATE userdata SET lasttx = ? WHERE address = ?`;
                                  con.query(sql2,[hash, address]);
                                  message.reply("Payment sent: https://blockscout.egem.io/tx/" + hash);
                                  let sql = `UPDATE userdata SET credits = ? WHERE userId = ?`;
                                  con.query(sql,[finalCreds, userId]);
                                  con.releaseConnection(connection);
                                });
                              })
                              .on('error', console.log);
                            }

                          }
                        } catch (e) {
                          message.reply("Payment failed.");
                          console.log(e)
                        }

                      })

                    } else {
                      // wd all
                      let sql = "SELECT credits,addressCreds FROM userdata WHERE userId = ?";
                      con.query(sql, userId, function (err, result){
                        try {
                          let credits = (result[0]['credits']/Math.pow(10,18)).toFixed(0);
                          let creditsBig = result[0]['credits'];
                          let address = result[0]['addressCreds'];

                          if (address == 0) {
                            return message.reply("No EGEM address to send payment to.")
                          }

                          if (credits <= 0) {
                            return message.reply("Payment failed, not enough to withdrawal, or error in request.");
                          } else {
                            let value = functions.numberToString(creditsBig);
                            let finalCreds = zero;

                            message.reply("Payment sending.");

                            web3.eth.sendTransaction({
                                from: botSettings.addressBot,
                                to: address,
                                gas: web3.utils.toHex(miscSettings.txgas),
                                value: web3.utils.toHex(value),
                                data: web3.utils.toHex(messageSend)
                            })
                            .on('transactionHash', function(hash){
                              con.getConnection(function(err, connection) {
                                if (err) throw err; // not connected!
                                let sql1 = "INSERT INTO txdatasent(`hash`,`to`,`value`) values(?,?,?)";
                                con.query(sql1,[hash,address,value]);
                                let sql2 = `UPDATE userdata SET lasttx = ? WHERE address = ?`;
                                con.query(sql2,[hash, address]);
                                message.reply("Payment sent: https://blockscout.egem.io/tx/" + hash);
                                let sql = `UPDATE userdata SET credits = ? WHERE userId = ?`;
                                con.query(sql,[zero, userId]);
                                con.releaseConnection(connection);
                              });
                            })
                            .on('error', console.log);

                          }
                        } catch (e) {
                          message.reply("Payment failed.");
                          console.log(e)
                        }
                      })

                    }
                  }
                }

              })
            })
        con.releaseConnection(connection);
      })
    } else {
      return message.reply("Private Message Only!");
    }

  }
}
