// Remove a node from a slot.
module.exports = {
  name: 'remnode',
  description: "Remove a node from a slot.",
  async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, mysql2, ipRegex, request, functions) {

    const con = mysql2.createPool({
      connectionLimit : 20,
      host: botSettings.mysqlip,
      user: botSettings.mysqluser,
      password: botSettings.mysqlpass,
      database: botSettings.mysqldb
    });

    const nodeNums = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20'];
    const tierNums = ['1'];

    const userId = message.author.id;
    const tier = args[0];
    const number = args[1];
    const extra = args[2];
    const zeroMe = "0";

    const ifNegT = Math.sign(tier);
    const ifNegN = Math.sign(number);

    if(message.channel.type === "DM") {

      if (!tierNums.includes(tier)) {
        return message.reply("Not a valid choice, tier must be a number 1.");
      }
      if (!nodeNums.includes(number)) {
        return message.reply("Not a valid choice, number must be a from 1-20.");
      }

      if (ifNegT == "-1" || ifNegN == "-1" || isNaN(tier) || isNaN(number) || extra != null) {
        return message.reply("Use properly or get banned.")
      }

      con.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
        try {
          let sql1 = "SELECT id FROM userdata WHERE userId = ?";
          con.query(sql1, userId, function (err, result1, fields){
            try {
              let id = result1[0]['id'];
              let sql2 = "SELECT * FROM usernodes"+tier+" WHERE id = ?";
              con.query(sql2, id, function (err, result2, fields){
                try {
                  let removeMe = result2[0]['n'+number]
                  console.log("Remove Me: "+removeMe)
                  let sql3 = "UPDATE usernodes"+tier+" SET n"+number+" =? WHERE id = ?"
                  con.query(sql3, [zeroMe,id]);
                  message.reply("Node removed.")
                } catch (e) {
                  console.log(e)
                }
              })
            } catch (e) {
              console.log(e)
            }
          })
        } catch (e) {
          console.log(e)
        }
        con.releaseConnection(connection);
      })
    } else {
      return message.reply("Private Message Only!");
    }

  }
}
