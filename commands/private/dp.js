module.exports = {
  name: 'dp',
  description: "Deposit to your tip and games balance.",
  async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3L, mysql2, ipRegex, request, functions) {

    var con = mysql2.createPool({
      connectionLimit : 10,
      host: botSettings.mysqlip,
      user: botSettings.mysqluser,
      password: botSettings.mysqlpass,
      database: botSettings.mysqldb
    });

    if(message.channel.type === "DM") {

      var tx = args[0];
      var extra = args[1];

      con.getConnection(function(err, connection) {
        if (err) throw err; // not connected!

        let sql1 = "SELECT creditsUse FROM usersystems";
        conPayout.query(sql1, function (err, data, fields){
          let inUse = data[0]['creditsUse'];
          if (inUse == 1) {
            return message.reply("System busy try again in a few minutes.");
          } else {

            if (tx == null) {
              return message.reply("Please include a tx.")
            }

            var resultTx = tx.match(/^0x[a-fA-F0-9]{64}$/)
            if (resultTx == null) {
              return message.reply("Not a valid tx foramt.")
            }
            console.log(resultTx)

            if (extra != null) {
              return message.reply("No junk please, my owner feeds me enough.")
            }

            web3L.eth.getTransaction(tx, (error,result)=>{
            if (error) {
              console.log(error);
            }
            if (result == null) {
              return message.reply("BAD BOY - You must include a EGEM transaction hash, sent from the registered address with bot.");
            }

            var to = result["to"];
            var from = result["from"];
            var valueRaw = result["value"];
            var minedBlock = result["blockNumber"];

            if (minedBlock == null) {
              const embed = new Discord.MessageEmbed()
                .setTitle("EGEM Discord Bot.")
                .setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })

                .setColor(miscSettings.warningcolor)
                .setDescription("Bot Deposit:")
                .setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })
                .setThumbnail(miscSettings.img32shard)

                .setTimestamp()
                .setURL(miscSettings.ghlink)
                .addField("Transaction not mined yet please wait.", "Please try again.")

              return message.reply({ embeds: [embed] })
            }

            var hash = args[0];

            let hashStr = hash;
            let fromStr = from.toString();
            let toStr = to.toString();
            let valueStr = valueRaw;

            console.log(toStr);
            if (web3L.utils.toChecksumAddress(toStr) !== botSettings.addressBot) {
              return message.reply("You can't claim a transaction that was not sent to the bot.");
            }

            con.query("SELECT addressCreds FROM userdata WHERE userId = ?", message.author.id, function (err, result) {
              if (err) console.log(err);
              var parsed = JSON.stringify(result);
              var obj = JSON.parse(parsed);

              var userAddy = obj[0]['addressCreds'];

              if (userAddy == 0) {
                return message.reply("You don't have a tip/rain address set.");
              }

              if (web3L.utils.toChecksumAddress(fromStr) !== web3L.utils.toChecksumAddress(userAddy)) {
                return message.reply("You have to send from your registered address to deposit.");
              }

              con.query("SELECT * FROM txdata WHERE hash = ?", hash, function (err, result) {
                if (err) console.log(err);
                con.query("SELECT * FROM txdatatopup WHERE hash = ?", hash, function (err, result2) {
                  if (err) console.log(err);
                try {
                  if (result == "" && result2 == "") {
                    var query = con.query("INSERT INTO txdata(`hash`,`from`,`to`,`value`) values(?,?,?,?)",[hash,from,to,valueRaw],function(err, result) {
                        console.log(valueRaw);
                        con.query("SELECT credits FROM userdata WHERE userId = ?", message.author.id, function (err, result) {
                          //console.log(result[0]['credits'])
                          var oldBal = parseInt(result[0]['credits']);
                          var newBal = parseInt(valueRaw);
                          var x = Number(oldBal);
                          var y = Number(newBal);
                          var balance = (x + y);
                          var balanceFinal = (balance/Math.pow(10,18)).toFixed(16);
                          con.query(`UPDATE userdata SET credits =? WHERE userId = ?`, [functions.numberToString(balance),message.author.id]);

                          //message.reply("TX registered. New Balance: " + functions.numberToString(balance) + " WEI | EGEM: " + balanceFinal );

                          const embed = new Discord.MessageEmbed()
                            .setTitle("Result:")
                            .setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })

                            .setColor(miscSettings.okcolor)
                            .setDescription("User Deposit:")
                            .setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })
                            .setThumbnail(miscSettings.img32shard)

                            .setTimestamp()
                            .setURL(miscSettings.ghlink)
                            .addField("You have deposited credits.", ""+(valueRaw/Math.pow(10,18)).toFixed(16))
                            .addField("Users new credits.", ""+balanceFinal)

                            message.reply({ embeds: [embed] })
                        })
                        //console.log(err);
                    });
                  } else {
                    return message.reply("TX registered already.");
                  }
                }catch(e){
                  console.log("ERROR ::",e)
                }

                if (err) throw err;
              })
            })
            })
          });
          con.releaseConnection(connection);
          }
        })

    })
    } else {
      return message.reply("Private Message Only!");
    }

  }
}
