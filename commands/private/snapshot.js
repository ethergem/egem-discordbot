module.exports = {
  name: 'snapshot',
  description: "",
  async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, mysql2, ipRegex, request, functions, mysql, web3R, userRoles, Duration, blacklist) {

    if(message.channel.type === "DM") {

      var address = args[0];
      if (address == null) {
        return message.reply("Please enter a valid EGEM address to lookup.")
      }

      let addycheck = web3R.utils.isAddress(address);

      if (addycheck !== true) {
        return message.reply("Not a valid EGEM address.")
      }

      web3R.eth.getBalance(address, 1530000)
      .then(function(result){
        //temp till sfrx release.
        var sfrxbal = ((result) * (2))/Math.pow(10,18);
        web3R.eth.getBalance(address, "latest")
        .then(function(result2){
          //temp till sfrx release.
          var egembal = result2/Math.pow(10,18);
          var sfrxbal = ((result) * (2))/Math.pow(10,18);
          message.reply("That address contains: " + egembal + " EGEM.")
          message.reply("Snapshot Balance: " + sfrxbal + " SFRX.")
        });
      });

    } else {
      return message.reply("Private Message Only!");
    }

  }
}