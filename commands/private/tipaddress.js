module.exports = {
  name: 'tipaddress',
  description: "",
  async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, mysql2, ipRegex, request, functions, mysql, web3R, userRoles, Duration, blacklist) {

    const con = mysql2.createPool({
      connectionLimit : 25,
      host: botSettings.mysqlip,
      user: botSettings.mysqluser,
      password: botSettings.mysqlpass,
      database: botSettings.mysqldb
    });

    if(message.channel.type === "DM") {
      con.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
        const author = message.author.id;
        const address = args[0];
        if (address == null) {
          return message.reply("Please provide a EGEM address.")
        }
        let addycheck = web3.utils.isAddress(address);

        if (blacklist.includes(address)) {
          return message.reply("Blacklisted address.")
        }
        if (addycheck !== true) {
          return message.reply("Not a valid EGEM address.")
        }
        let sql1 = `SELECT addressCreds FROM userdata`;
        con.query(sql1, function (err, blacklist) {
          if (err) return message.reply("No Results.");
          let parsed = JSON.stringify(blacklist);
          let obj = JSON.parse(parsed);
          //console.log(parsed);
          con.query("SELECT addressCreds FROM userdata WHERE userId = ?", author, function (err, result) {
            if(!err) {
              let parsed1 = JSON.stringify(result);
              let obj1 = JSON.parse(parsed1);
              try {
                if (parsed.includes(address)) {
                  return message.reply("Address already registered by someone else.")
                } else {
                  let sql2 = `UPDATE userdata SET addressCreds = ? WHERE userId = ?`;
                  con.query(sql2, [address, author]);
                  return message.reply("Address set on account.")
                }
              } catch (e) {
                console.log(e);
                return message.reply("Set error.")
              }
            } else {
              console.log(err);
              return message.reply("Set error.")
            }
          })
          con.releaseConnection(connection);
        })
      })
    }else {
      return message.reply("Private Message Only!");
    }

  }
}
