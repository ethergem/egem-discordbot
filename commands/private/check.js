// Check an IP of a EGEM node to see if it is online.
module.exports = {
  name: 'check',
  description: "Check to see if a node is online.",
  async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, mysql2, ipRegex, request, functions, mysql, web3R, userRoles, Duration, blacklist) {
    var ip = args[0];
    var extra = args[1];

    if(message.channel.type === "DM") {
      try {
        if (ip == null) {
          return message.reply("Fill in all fields: /check ip");
        }

        if (extra != null) {
          return message.reply("No junk please, my owner feeds me enough.")
        }

        let ipCheck = ipRegex({exact: true}).test(ip);
        if (ipCheck != true) {
          return message.reply("Not a valid choice, must be a valid ip.")
        }

        var thebooty = {"jsonrpc":"2.0","method":"net_peerCount","params":[],"id":1};

        message.reply("Seeing if online please wait...")

        request({
            url: "http://"+ip+":8895",
            method: "POST",
            json: true,
            body: thebooty
        }, function (error, response, body){
            if (!error) {
              var peers = web3.utils.hexToNumber(body.result);
              message.reply("Yup, node is online with "+peers+" peers.")
            } else {
              console.log(error)
              message.reply("Node is offline, or port blocked, or command not avaliable.")
            }
        });

      } catch (e) {
        console.log(e);
      }
    } else {
      return message.reply("Private Message Only!");
    }

  }
}
