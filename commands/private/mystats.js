module.exports = {
  name: 'mystats',
  description: "ping stats of a node",
  async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, mysql2, ipRegex, request, functions) {

    var con = mysql2.createPool({
      connectionLimit : 100,
      host: botSettings.mysqlip,
      user: botSettings.mysqluser,
      password: botSettings.mysqlpass,
      database: botSettings.mysqldb
    });

    var author = message.author.id;

    if(message.channel.type === "DM") {
      con.getConnection(function(err, connection) {
          if (err) throw err; // not connected!
          con.query("SELECT id FROM userdata WHERE userId = ?", author, function (err, result1, fields){
            try {
              let id = result1[0]['id'];
              con.query("SELECT n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15,n16,n17,n18,n19,n20 FROM usernodes1 WHERE id=?", id, function (err, result) {
                var i;
                message.reply("Please wait while tier one stats are pinged.");
                for (i = 1; i <= 20; i++) {
                  try {
                    var ip = result[0]['n'+i+''];
                    if (ip == 0) {
                      //return;
                    } else {
                      async function getStats(ip,i) {
                        await request({
                            url: "http://"+ip+":8897/stats",
                            method: "GET",
                            json: true
                        }, function (error, response, body){
                            if (!error) {
                              try {
                                const embed = new Discord.MessageEmbed()
                                  .setTitle("Result:")
                                  .setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })

                                  .setColor(miscSettings.okcolor)
                                  .setDescription("My Stats:")
                                  .setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })
                                  .setThumbnail(miscSettings.img32shard)

                                  .setTimestamp()
                                  .setURL(miscSettings.ghlink)
                                  .addField("IP Address:",ip)
                                  .addField("Node Version:",body['client_version'])
                                  .addField("Host Name:",body['hostname'])
                                  .addField("Current Block:",body['current_block']+"")
                                  .addField("Uptime (Mins):",body['host_uptime_mins']+"")
                                  .addField("Free Space:",body['disk_free_mb']+" mb")
                                  .addField("Mem. Used:",(body['phy_mem_used_percent']).toFixed(2)+" %")
                                  .addField("Swap. Used:",(body['swp_mem_used_percent']).toFixed(2)+" %")

                                  message.reply({ embeds: [embed] })
                              } catch (e) {
                                console.log(e);
                              }
                            } else {
                              message.reply("Can't connect to the stats endpoint of your tier one node: "+ip)
                            }
                        });
                      }
                      getStats(ip,i)
                    }
                  } catch (e) {
                    console.log(e)
                  }
                }
              })

            } catch (e) {
              console.log(e)
            }
          })
          con.releaseConnection(connection);
      });
    }else {
      return message.reply("Private Message Only!");
    }

  }
}
