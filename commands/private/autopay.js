// Toggle autopay on/off of earned node funds.
module.exports = {
  name: 'autopay',
  description: "Toggle autopay.",
  async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, mysql2) {

      var user = message.author.username;
      var author = message.author.id;

      var con = mysql2.createPool({
        connectionLimit : 25,
        host: botSettings.mysqlip,
        user: botSettings.mysqluser,
        password: botSettings.mysqlpass,
        database: botSettings.mysqldb
      });

      if(message.channel.type === "DM") {
        con.getConnection(function(err, connection) {
          if (err) throw err; // not connected!
          con.query("SELECT autopay, userId FROM userdata WHERE userId = ?", author, function (err, result) {
            if (err) return message.reply("No Results.");
            try {
              let parsed = JSON.stringify(result);
              let obj = JSON.parse(parsed);
              let authorCheck = obj[0]["userId"];
              let autoWithdrawals = obj[0]["autopay"];
              if (authorCheck == author) {
                if (author == authorCheck) {
                  if (autoWithdrawals == "1") {
                    let response = "0";
                    con.query(`UPDATE userdata SET autopay =? WHERE userId = ?`, [response,author]);
                    return message.reply("Auto pay updated to No, User must manually withdrawal node earnings.");
                  }
                  if (autoWithdrawals == "0") {
                    let response = "1";
                    con.query(`UPDATE userdata SET autopay =? WHERE userId = ?`, [response,author]);
                    return message.reply("Auto pay updated to Yes, system will send payments from node earnings once threshold is reached.");
                  }
                  //console.log(obj);
                }
              }
            }catch(e){
              console.log("ERROR ::",e)
              con.releaseConnection(connection);
              return message.reply("Not registered, use /botreg <address>.");
            }
          })
        })
      } else {
        return message.reply("Private Message Only!");
      }
  }
}
