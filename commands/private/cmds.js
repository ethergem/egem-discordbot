// .
module.exports = {
  name: 'cmds',
  description: "List of commands for private message.",
  async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, ipRegex, request) {

    if(message.channel.type === "DM") {
      try {
        const private = './commands/private/';
        const help = './commands/help/';
        const fs = require('fs');
        const cmdArray = [];

        fs.readdir(private, (err, files) => {
          files.forEach(file => {
            // console.log(file);
            // strip .js
            let cleanCmd = file.slice(0, file.length - 3);
            // add to array
            cmdArray.push(cleanCmd);
          });
          fs.readdir(help, (err, files) => {
            files.forEach(file => {
              console.log(file);
              // strip .js
              let cleanCmd = file.slice(0, file.length - 3);
              // add to array
              cmdArray.push(cleanCmd);
            });
            // show cmds
            console.log(cmdArray);
            message.reply("Commands: "+cmdArray.sort());
          });
        });

      } catch (e) {
        console.log(e);
      }
    } else {
      return message.reply("Private Message Only!");
    }

  }
}
