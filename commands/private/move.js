module.exports = {
  name: 'move',
  description: "Move earnings back and forth depending on need.",
  async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, mysql2, ipRegex, request, functions) {

    var con = mysql2.createPool({
      connectionLimit : 10,
      host: botSettings.mysqlip,
      user: botSettings.mysqluser,
      password: botSettings.mysqlpass,
      database: botSettings.mysqldb
    });

    if(message.channel.type === "DM") {

        var userId = message.author.id;
        var direction = args[0];
        var moveamt = args[1];
        var extra = args[2];
        var zero = 0;

        if (extra != null) {
          return message.reply("No junk please, my owner feeds me enough.")
        }

        con.getConnection(function(err, connection) {
          if (err) throw err; // not connected!
          let sql1 = "SELECT creditsUse FROM usersystems";
          con.query(sql1, function (err, data, fields){
            var inUse = data[0]['creditsUse'];
            if (inUse == 1) {
              return message.reply("Please try again in 1 minute, background actions being done currently.");
            } else {
              if (moveamt) {
                if (direction === "in") {
                  let sql = "SELECT credits,mnrewards FROM userdata WHERE userId = ?";
                  con.query(sql, userId, function (err, result){
                    try {
                      let credits = (result[0]['credits']/Math.pow(10,18)).toFixed(0);
                      let creditsBig = result[0]['credits'];

                      let mnrewards = (result[0]['mnrewards']/Math.pow(10,18)).toFixed(0);
                      let mnrewardsBig = result[0]['mnrewards'];

                      let moveamtBig = moveamt*Math.pow(10,18);

                      console.log(credits);
                      console.log(mnrewards);

                      let ifNeg = Math.sign(moveamt);

                      if (mnrewards <= 0 || mnrewardsBig < moveamtBig || ifNeg == "-1" || isNaN(moveamt)) {
                        return message.reply("Move failed, not enough to transfer, or error in request.");
                      } else {

                        let mnFin = (Number(mnrewardsBig) - Number(moveamtBig));
                        let credsFin = (Number(creditsBig) + Number(moveamtBig));

                        console.log(mnFin/Math.pow(10,18));
                        console.log(credsFin/Math.pow(10,18));

                        let sql1 = `UPDATE userdata SET credits = ? WHERE userId = ?`;
                        con.query(sql1,[credsFin, userId]);

                        let sql2 = `UPDATE userdata SET mnrewards = ? WHERE userId = ?`;
                        con.query(sql2,[mnFin, userId]);

                        const embed = new Discord.MessageEmbed()
                          .setTitle("Result:")
                          .setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })

                          .setColor(miscSettings.okcolor)
                          .setDescription("Funds Move:")
                          .setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })
                          .setThumbnail(miscSettings.img32shard)

                          .setTimestamp()
                          .setURL(miscSettings.ghlink)
                          .addField("Moved to Credits:", ""+moveamt+" EGEM")
                          .addField("Credits Balance.", ""+credsFin/Math.pow(10,18)+" EGEM")
                          .addField("Node Balance Left:", ""+mnFin/Math.pow(10,18)+" EGEM")

                          message.reply({ embeds: [embed] })

                        con.releaseConnection(connection);
                        return;

                      }
                    } catch (e) {
                      message.reply("Move failed.");
                      console.log(e)
                    }

                  });
                } else if (direction === "out") {
                  let sql = "SELECT credits,mnrewards FROM userdata WHERE userId = ?";
                  con.query(sql, userId, function (err, result){
                    try {
                      let credits = (result[0]['credits']/Math.pow(10,18)).toFixed(0);
                      let creditsBig = result[0]['credits'];

                      let mnrewards = (result[0]['mnrewards']/Math.pow(10,18)).toFixed(0);
                      let mnrewardsBig = result[0]['mnrewards'];

                      let moveamtBig = moveamt*Math.pow(10,18);

                      console.log(credits);
                      console.log(mnrewards);

                      let ifNeg = Math.sign(moveamt);

                      if (credits <= 0 || creditsBig < moveamtBig || ifNeg == "-1" || isNaN(moveamt)) {
                        return message.reply("Move failed, not enough to transfer, or error in request.");
                      } else {

                        let mnFin = (Number(mnrewardsBig) + Number(moveamtBig));
                        let credsFin = (Number(creditsBig) - Number(moveamtBig));

                        console.log(mnFin/Math.pow(10,18));
                        console.log(credsFin/Math.pow(10,18));

                        let sql1 = `UPDATE userdata SET credits = ? WHERE userId = ?`;
                        con.query(sql1,[credsFin, userId]);

                        let sql2 = `UPDATE userdata SET mnrewards = ? WHERE userId = ?`;
                        con.query(sql2,[mnFin, userId]);

                        const embed = new Discord.MessageEmbed()
                          .setTitle("Result:")
                          .setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })

                          .setColor(miscSettings.okcolor)
                          .setDescription("Funds Move:")
                          .setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })
                          .setThumbnail(miscSettings.img32shard)

                          .setTimestamp()
                          .setURL(miscSettings.ghlink)
                          .addField("Moved to Nodes:",""+moveamt+" EGEM")
                          .addField("Node Balance.", ""+mnFin/Math.pow(10,18)+" EGEM")
                          .addField("Credits Balance Left:", ""+credsFin/Math.pow(10,18)+" EGEM")

                          message.reply({ embeds: [embed] })

                        con.releaseConnection(connection);
                        return;

                      }
                    } catch (e) {
                      message.reply("Move failed.");
                      console.log(e)
                    }

                  })
                } else {
                  return message.reply("Direction is 'in/out' to game/tip credits or 'in/out' to node rewards")
                }
              } else {
                return message.reply("Move failed, not enough to transfer, or error in request.");
              }
            }
          })
          con.releaseConnection(connection);
        })
    } else {
      return message.reply("Private Message Only!");
    }

  }
}
