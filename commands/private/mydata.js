module.exports = {
  name: 'mydata',
  description: "review data stored in bot",
  async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, mysql2, ipRegex, request, functions) {

    var con = mysql2.createPool({
      connectionLimit : 20,
      host: botSettings.mysqlip,
      user: botSettings.mysqluser,
      password: botSettings.mysqlpass,
      database: botSettings.mysqldb
    });

    var userId = message.author.id;

    if(message.channel.type === "DM") {
      con.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
        let sql1 = "SELECT id,address,addressCreds,avatar,paylimit,userpay,numberOfWDAmount,numberOfWD,lasttx FROM userdata WHERE userId = ?";
        con.query(sql1, userId, function (err, result, fields){
          try {
            if (err) throw err; // not connected!
            var botId = result[0]['id'];
            var address = result[0]['address'];
            var addressCreds = result[0]['addressCreds'];
            var paylimit = result[0]['paylimit'];
            var wDAmount = result[0]['numberOfWDAmount'];
            var numberOfWD = result[0]['numberOfWD'];
            var userpay = result[0]['userpay'];
            var lasttx = result[0]['lasttx']

            if (lasttx !== "0") {
              const embed = new Discord.MessageEmbed()
                .setTitle("Result:")
                .setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })

                .setColor(miscSettings.okcolor)
                .setDescription("My Data:")
                .setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })
                .setThumbnail(miscSettings.img32shard)

                .setTimestamp()
                .setURL(miscSettings.ghlink)
                .addField("Node Address:",""+address)
                .addField("Tip Address:",""+addressCreds)
                .addField("Bot ID:",""+botId)
                .addField("Discord ID:",""+userId)
                .addField("Pay Limit:",""+paylimit)
                .addField("Earnings Per 15mins:",(userpay/Math.pow(10,18))+" EGEM")
                .addField("Last Payment:", "https://blockscout.egem.io/tx/"+ lasttx)
                .addField("Amount Withdrawn",(wDAmount/Math.pow(10,18))+" EGEM")
                .addField("Withdrawals Count",""+numberOfWD)

                message.reply({ embeds: [embed] })
            } else {
              const embed = new Discord.MessageEmbed()
                .setTitle("Result:")
                .setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })

                .setColor(miscSettings.okcolor)
                .setDescription("My Data:")
                .setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })
                .setThumbnail(miscSettings.img32shard)

                .setTimestamp()
                .setURL(miscSettings.ghlink)
                .addField("Node Address:",""+address)
                .addField("Tip Address:",""+addressCreds)
                .addField("Discord ID:",""+userId)
                .addField("Pay Limit:",""+paylimit)
                .addField("Earnings Per 15mins:",(userpay/Math.pow(10,18))+" EGEM")
                .addField("Amount Withdrawn",(wDAmount/Math.pow(10,18))+" EGEM")
                .addField("Withdrawals Count",""+numberOfWD)

                return message.reply({ embeds: [embed] })
            }

          } catch (e) {
            console.log(e)
            message.reply("User Not registered, use /register.");
          }
        })
      con.releaseConnection(connection);
      })
    }else {
      return message.reply("Private Message Only!");
    }

  }
}
