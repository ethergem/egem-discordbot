//
module.exports = {
  name: 'mybal',
  description: "Check the balance stored within bot.",
  async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, mysql2) {

      var userId = message.author.id;

      var con = mysql2.createPool({
        connectionLimit : 2,
        host: botSettings.mysqlip,
        user: botSettings.mysqluser,
        password: botSettings.mysqlpass,
        database: botSettings.mysqldb
      });

      if(message.channel.type === "DM") {
        con.getConnection(function(err, connection) {
          if (err) throw err; // not connected!
          try {
            con.query("SELECT avgusd FROM usersystems WHERE id = '1'", userId, function (err, data, fields){
              if (err) throw err; // not connected!
              var avgusd = data[0]['avgusd'];
              con.query("SELECT balance,balanceTwo,credits,mnrewards FROM userdata WHERE userId = ?", userId, function (err, result, fields){
                try {
                  if (err) throw err; // not connected!
                  var balance0 = result[0]['balance'];
                  var worth0 = Number(Number(balance0) *Number(avgusd));
                  var balance1 = result[0]['balanceTwo'];
                  var worth1 = Number(Number(balance1) *Number(avgusd));
                  var credits = (result[0]['credits']/Math.pow(10,18));
                  var worth2 = Number(Number(credits) *Number(avgusd));
                  var mnrewards = (result[0]['mnrewards']/Math.pow(10,18));
                  var worth3 = Number(Number(mnrewards) *Number(avgusd));

                  const embed = new Discord.MessageEmbed()
                    .setTitle("Result:")
                    .setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })

                    .setColor(miscSettings.okcolor)
                    .setDescription("My Balances:")
                    .setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })
                    .setThumbnail(miscSettings.img32shard)

                    .setTimestamp()
                    .setURL(miscSettings.ghlink)
                    .addField("Node Wallet:",Number(balance0).toFixed(8)+" EGEM. ($"+Number(worth0).toFixed(4)+" USD)")
                    .addField("Node Credits:",Number(mnrewards).toFixed(8)+" EGEM. ($"+Number(worth3).toFixed(4)+" USD)")
                    .addField("Tip Wallet:",Number(balance1).toFixed(8)+" EGEM. ($"+Number(worth1).toFixed(4)+" USD)")
                    .addField("Tip Credits:",Number(credits).toFixed(8)+" EGEM. ($"+Number(worth2).toFixed(4)+" USD)")

                    //message.reply({embed})
                    message.reply({ embeds: [embed] })
                } catch (e) {
                  console.log(e)
                  message.reply("User Not registered, use /register.");
                }
                con.end();
              })
            })
          } catch (e) {
            console.log(e)
          }
          con.releaseConnection(connection);
        })
      } else {
        return message.reply("Private Message Only!");
      }

  }
}
