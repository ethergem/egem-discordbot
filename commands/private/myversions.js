// .
module.exports = {
  name: 'myversions',
  description: "List of node versions registered in system.",
  async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, mysql2, ipRegex, request, functions) {

    var con = mysql2.createPool({
      connectionLimit : 20,
      host: botSettings.mysqlip,
      user: botSettings.mysqluser,
      password: botSettings.mysqlpass,
      database: botSettings.mysqldb
    });

    if(message.channel.type === "DM") {
      try {
        let userId = message.author.id;

        con.getConnection(function(err, connection) {
          if (err) throw err; // not connected!
          con.query("SELECT id FROM userdata WHERE userId = ?", userId, function (err, result1, fields){
            try {
              let id = result1[0]['id'];
              if (id == 'undefined' || id == null) {
                return message.reply("Not registered!")
              }
              if (err) throw err; // not connected!
              con.query("SELECT * FROM usernodes1 WHERE id = ?", id, function (err, result2, fields){
                if (err) throw err; // not connected!
                var count1 = result2[0]['count'];
                let t1v1 = result2[0]['v1'];
                let t1v2 = result2[0]['v2'];
                let t1v3 = result2[0]['v3'];
                let t1v4 = result2[0]['v4'];
                let t1v5 = result2[0]['v5'];
                let t1v6 = result2[0]['v6'];
                let t1v7 = result2[0]['v7'];
                let t1v8 = result2[0]['v8'];
                let t1v9 = result2[0]['v9'];
                let t1v10 = result2[0]['v10'];
                let t1v11 = result2[0]['v11'];
                let t1v12 = result2[0]['v12'];
                let t1v13 = result2[0]['v13'];
                let t1v14 = result2[0]['v14'];
                let t1v15 = result2[0]['v15'];
                let t1v16 = result2[0]['v16'];
                let t1v17 = result2[0]['v17'];
                let t1v18 = result2[0]['v18'];
                let t1v19 = result2[0]['v19'];
                let t1v20 = result2[0]['v20'];


                console.log(count1);

                const embed = new Discord.MessageEmbed()
                  .setTitle("Result:")
                  .setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })

                  .setColor(miscSettings.okcolor)
                  .setDescription("My Nodes:")
                  .setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })
                  .setThumbnail(miscSettings.img32shard)

                  .setTimestamp()
                  .setURL(miscSettings.ghlink)
                  .addField("Node Count:",Number(count1)+" Online.")
                  .addField("Version 1: ", t1v1)
                  .addField("Version 2: ", t1v2)
                  .addField("Version 3: ", t1v3)
                  .addField("Version 4: ", t1v4)
                  .addField("Version 5: ", t1v5)
                  .addField("Version 6: ", t1v6)
                  .addField("Version 7: ", t1v7)
                  .addField("Version 8: ", t1v8)
                  .addField("Version 9: ", t1v9)
                  .addField("Version 10: ", t1v10)
                  .addField("Version 11: ", t1v11)
                  .addField("Version 12: ", t1v12)
                  .addField("Version 13: ", t1v13)
                  .addField("Version 14: ", t1v14)
                  .addField("Version 15: ", t1v15)
                  .addField("Version 16: ", t1v16)
                  .addField("Version 17: ", t1v17)
                  .addField("Version 18: ", t1v18)
                  .addField("Version 19: ", t1v19)
                  .addField("Version 20: ", t1v20)

                return message.reply({ embeds: [embed] })

              })
            } catch (e) {
              console.log(e)
              message.reply("User Not registered, use /register.");
            }
          })
          con.releaseConnection(connection);
        })
      } catch (e) {
        console.log(e);
      }
    } else {
      return message.reply("Private Message Only!");
    }

  }
}
