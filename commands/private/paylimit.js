module.exports = {
  name: 'paylimit',
  description: "",
  async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, mysql2, ipRegex, request, functions) {

    const con = mysql2.createPool({
      connectionLimit : 20,
      host: botSettings.mysqlip,
      user: botSettings.mysqluser,
      password: botSettings.mysqlpass,
      database: botSettings.mysqldb
    });

    if(message.channel.type === "DM") {

      con.getConnection(function(err, connection) {
        let author = message.author.id;
        let paylimit = args[0];
        let ifNeg = Math.sign(paylimit);
        let extra = args[1];

        if (err) throw err; // not connected!

        if (paylimit > 100 || paylimit < 1 || isNaN(paylimit) || paylimit == null || ifNeg == "-1" || extra != null) {
          return message.reply("Please pick a number from 1 min - 100 max EGEM.")
        } else {
          let sql2 = "SELECT paylimit FROM userdata WHERE userId = ?";
          con.query(sql2, author, function (err, result) {
            try {
              if (!result) return message.reply("User Not registered, use /register.");
              let parsed = JSON.stringify(result);
              let obj = JSON.parse(parsed);
              let sql3 = `UPDATE userdata SET paylimit = ? WHERE userId = ?`;
              con.query(sql3, [paylimit, author]);
              message.reply("Pay limit updated to: "+ paylimit)

              con.releaseConnection(connection);
            } catch (e) {
              console.log(e)
            }
          })
        }
      })
    } else {
      return message.reply("Private Message Only!");
    }

  }
}
