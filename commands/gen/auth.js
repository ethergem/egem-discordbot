module.exports = {
  name: 'auth',
  description: "Allows a user to self auth to see rest of server.",
  async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, mysql2, ipRegex, request, functions, mysql, web3R, userRoles) {

    let role = message.guild.roles.cache.find(r => r.id === userRoles.egemregular);

    let member = message.member;

    if (message.member.roles.cache.has(role.id)) {
      message.reply("You already have access to the rest of server.");
    } else {
      message.reply("You are now verified thank you and enjoy access to the rest of server.");
      member.roles.add(role).catch(console.error);
    }

  }
}
