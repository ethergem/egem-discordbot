const { MessageEmbed } = require('discord.js');
const { SlashCommandBuilder } = require('@discordjs/builders');
const talkedRecentlyFaucet = new Map();

module.exports = {
  name: 'leech',
  description: "Send a small amount of EGEM to a address!",
	async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, mysql2, ipRegex, request, functions, mysql, web3R, userRoles, Duration, blacklist) {

		var con = mysql2.createPool({
			connectionLimit : 20,
			host: botSettings.mysqlip,
			user: botSettings.mysqluser,
			password: botSettings.mysqlpass,
			database: botSettings.mysqldb
		});


		if(message.channel.id === botChans.faucetRoom) {

			// check if funds in wallet to send tip
			// TODO
			con.getConnection(function(err, connection) {
				if (err) throw err; // not connected!
				let sql8 = "SELECT totalCredits,totalMNRewards FROM usersystems";
				con.query(sql8, function (err, data, fields){
					var creds = Number(data[0]['totalCredits']/Math.pow(10,18)).toFixed(0);
					var mnrew = Number(data[0]['totalMNRewards']/Math.pow(10,18)).toFixed(0);
					var totalCredits = Number(Number(creds) + Number(mnrew));

					web3.eth.getBalance(botSettings.addressBot, "latest")
					.then(function(results){
						var botfunds = Number(results)/Math.pow(10,18).toFixed(0);
						console.log(botfunds);
						console.log(totalCredits);

						if (botfunds > totalCredits) {
							console.log("sending");

								    let userAddress = args[0];
								    let addycheck = web3.utils.isAddress(userAddress);
								    let amount = (Math.random() * (0.22 - 0.06) + 0.07).toFixed(8);
								    let weiAmount = amount*Math.pow(10,18);
								    let finValue = functions.numberToString(weiAmount);

								    const cooldown = talkedRecentlyFaucet.get(message.author.id);

								    if (cooldown) {

								      const remaining = Duration(cooldown - Date.now(), { units: ['h', 'm'], round: true})

								      const embed = new Discord.MessageEmbed()
								        .setTitle("EGEM Discord Bot.")
								        .setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })

								        .setColor(miscSettings.warningcolor)
								        .setDescription("Faucet:")
								        .setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })
								        .setThumbnail(miscSettings.img32shard)

								        .setTimestamp()
								        .setURL(miscSettings.ghlink)
								        .addField("Error:","Cool down triggered.")
								        .addField("Time Remaining", remaining)

								        return message.reply({ embeds: [embed] })

								    } else {

								      if (!userAddress) {
								        const embed = new Discord.MessageEmbed()
								          .setTitle("EGEM Discord Bot.")
								          .setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })

								          .setColor(miscSettings.warningcolor)
								          .setDescription("Faucet:")
								          .setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })
								          .setThumbnail(miscSettings.img32shard)

								          .setTimestamp()
								          .setURL(miscSettings.ghlink)
								          .addField("Error:","No EGEM address provided, can not continue.")

								          return message.reply({ embeds: [embed] })
								      }

								      if (addycheck !== true) {
								        const embed = new Discord.MessageEmbed()
								          .setTitle("EGEM Discord Bot.")
								          .setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })

								          .setColor(miscSettings.warningcolor)
								          .setDescription("Faucet:")
								          .setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })
								          .setThumbnail(miscSettings.img32shard)

								          .setTimestamp()
								          .setURL(miscSettings.ghlink)
								          .addField("Error:","Not a valid EGEM address, can not continue.")

								          return message.reply({ embeds: [embed] })
								      }

								      if (blacklist.includes(userAddress)) {
								        const embed = new Discord.MessageEmbed()
								          .setTitle("EGEM Discord Bot.")
								          .setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })

								          .setColor(miscSettings.warningcolor)
								          .setDescription("Faucet:")
								          .setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })
								          .setThumbnail(miscSettings.img32shard)

								          .setTimestamp()
								          .setURL(miscSettings.ghlink)
								          .addField("Error:","Blacklisted address, can not continue.")

								          return message.reply({ embeds: [embed] })
								      }

											try {

												sendCoins(userAddress, finValue);

											} catch (e) {
												console.log(e);
											}

											function sendCoins(userAddress, finValue) {
												web3.eth.sendTransaction({
												    from: botSettings.addressBot,
												    to: userAddress,
												    gas: web3.utils.toHex(miscSettings.txgas),
												    value: web3.utils.toHex(finValue),
												    data: web3.utils.toHex(miscSettings.messageSend)
												})
												.on('transactionHash', function(hash){

													talkedRecentlyFaucet.set(message.author.id, Date.now() + 1000 * 60 * 60 * 4);
													setTimeout(() => { talkedRecentlyFaucet.delete(message.author.id)}, 1000 * 60 * 60 * 4);

												  const embed = new Discord.MessageEmbed()
												    .setTitle("EGEM Discord Bot.")
												    .setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })

												    .setColor(miscSettings.okcolor)
												    .setDescription("Faucet:")
												    .setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })
												    .setThumbnail(miscSettings.img32shard)

												    .setTimestamp()
												    .setURL(miscSettings.ghlink)
												    .addField("User:",userAddress)
												    .addField("Amount:", amount+" EGEM")
												    .addField("Faucet Hash:","https://blockscout.egem.io/tx/" + hash)

												    return message.reply({ embeds: [embed] })
												})
												.on('error', console.log);
											}
								    }
						} else {

							return message.reply("No funds to give to users please send "+Number((totalCredits - botfunds) + 500)+" to: "+botSettings.addressBot)
						}

					});
          con.end();
				})
				//con.releaseConnection(connection);
			})
			//

		} else {
			message.reply("Wrong room!")
		}

	},
};
