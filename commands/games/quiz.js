const quiz = require('../../cfgs/trivia/quiz.json');
const { egemspin,okcolor,warningcolor,footerBranding,img32x32,img32shard,ghlink,web3IPC,web3HTTP,txgas,messageSend } = require('../../cfgs/settings.json');
const talkedRecently = new Set();

module.exports = {
  name: 'quiz',
  description: "Run a quiz where winner earns some egem.",
  async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, mysql2, ipRegex, request, functions, mysql) {

    var con = mysql.createConnection({
      host: botSettings.mysqlip,
      user: botSettings.mysqluser,
      password: botSettings.mysqlpass,
      database: botSettings.mysqldb
    });

    var prize = args[0];
    var timeLimit = Number(args[1]*1000);
    var talkCooldown = 60000;

    if (talkedRecently.has(message.author.id)) {
        message.reply("Wait "+(talkCooldown/1000)+" seconds before playing again.");
    } else {

      if(message.channel.id === botChans.triviaroom) {
        if (!admins.includes(message.author.id)) {
          return message.reply("Not authorized.")
        }

        // NEED MAX TIMER
        // NEED MAX WIN

        var ifNeg = Math.sign(prize);
        if (ifNeg == "-1") {
          return message.reply("You can't use a negative amount, Abuse will result in a disabled account.");
        }

        const item = quiz[Math.floor(Math.random() * quiz.length)];
        const filter = response => {
          return item.answers.some(answer => answer.toLowerCase() === response.content.toLowerCase());
        };

        if (!prize) {
          let amount = (Math.random() * (0.75 - 0.06) + 0.07).toFixed(8);
          prize = amount;
        }

        if (!timeLimit) {
          timeLimit = 25000;
        }

        if(isNaN(prize) || isNaN(timeLimit)){
         return message.reply("That is not a valid number, Abuse will result in a disabled account.");
        }

        function sendEgem(winnerAddress,finValue,prize){
          web3.eth.sendTransaction({
              from: botSettings.addressBot,
              to: winnerAddress,
              gas: web3.utils.toHex(miscSettings.txgas),
              value: web3.utils.toHex(finValue),
              data: web3.utils.toHex(miscSettings.messageSend)
          })
          .on('transactionHash', function(hash){

              const embed = new Discord.MessageEmbed()
                .setColor(okcolor)
                .setTitle("EGEM Discord Bot.")
                .setURL(ghlink)
                .setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })
                .setDescription('Quiz Rewards:')
                .setThumbnail(img32shard)
                .addFields(
                  { name: 'User:', value: winnerAddress },
                  { name: 'Amount:', value: prize+' EGEM ' },
                  { name: 'Hash:', value: 'https://blockscout.egem.io/tx/' + hash},
                )
                .setTimestamp()
                .setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })

              message.reply({ embeds: [embed] });
          })
          .on('error', console.log);

        }

        //

        const embedQuiz = new Discord.MessageEmbed()
          .setColor(okcolor)
          .setTitle("EGEM Discord Bot.")
          .setURL(ghlink)
          .setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })
          .setDescription('Quiz:')
          .setThumbnail(img32shard)
          .addFields(
            { name: 'Question: :question:', value: item.question },
            { name: 'Prize: :gift:', value: `${prize} EGEM`},
            { name: 'Time: :timer:', value: `${timeLimit/1000} Seconds left.`},
          )
          .setTimestamp()
          .setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })

        //

        const embedQuizFail = new Discord.MessageEmbed()
          .setColor(warningcolor)
          .setTitle("EGEM Discord Bot.")
          .setURL(ghlink)
          .setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })
          .setDescription('Quiz:')
          .setThumbnail(img32shard)
          .addFields(
            { name: 'Failed:', value: 'Looks like nobody got the answer this time.' },
            { name: 'Answer:', value: `${item.answers}` },
          )
          .setTimestamp()
          .setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })

        message.reply({ embeds: [embedQuiz] }, { fetchReply: true })
          .then(() => {
            message.channel.awaitMessages({ filter, max: 1, time: timeLimit, errors: ['time'] })
              .then(collected => {
                var winner = collected.first().author;

                const embedQuizWin = new Discord.MessageEmbed()
                  .setColor(okcolor)
                  .setTitle("EGEM Discord Bot.")
                  .setURL(ghlink)
                  .setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })
                  .setDescription('Quiz Rewards:')
                  .setThumbnail(img32shard)
                  .addFields(
                    { name: 'Winner: :partying_face:', value: `${collected.first().author} got the correct answer!` },
                    { name: 'Prize:', value: `${prize} EGEM`},
                  )
                  .setTimestamp()
                  .setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })

                message.reply({ embeds: [embedQuizWin] });

                con.connect(function(err) {
                  if (err) {
                    console.error('error connecting: ' + err.stack);
                    return;
                  }
                  console.log('connected as id ' + con.threadId);
                });

                con.query("SELECT addressCreds FROM userdata WHERE userId = ?", winner.id, function (err, result, fields){
                  if (err || result[0]['addressCreds'] === "0") {
                    message.reply(`${collected.first().author} Doesn't have a EGEM address set in bot for sending rewards to, or an error happened.`)
                    console.log(err);
                  } else {
                    var winnerAddress = result[0]['addressCreds'];
                    let weiAmount = prize*Math.pow(10,18);
                    let finValue = functions.numberToString(weiAmount);
                    console.log(winnerAddress,finValue);
                    sendEgem(winnerAddress,finValue,prize)
                  }

                })
                con.end(function(err) {
                  console.log('disconnected as id ' + con.threadId);
                });
              })
              .catch(collected => {

                message.reply({ embeds: [embedQuizFail] });

                con.end(function(err) {
                  console.log('disconnected as id ' + con.threadId);
                });
              });
          });
      } else {
        return message.reply("Use in trivia room.")
      }

        // Adds the user to the set so that they can't talk for 1 minute
        talkedRecently.add(message.author.id);
        setTimeout(() => {
          // Removes the user from the set after 1 minute
          talkedRecently.delete(message.author.id);
        }, talkCooldown);
    }

  }
}
