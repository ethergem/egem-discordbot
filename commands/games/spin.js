const { egemspin,okcolor,warningcolor,footerBranding,img32x32,img32shard,ghlink,web3IPC,web3HTTP,txgas,messageSend } = require('../../cfgs/settings.json');
const talkedRecently = new Set();

module.exports = {
  name: 'spin',
  description: "",
  async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, mysql2, ipRegex, request, functions, mysql) {

    var con = mysql.createConnection({
      host: botSettings.mysqlip,
      user: botSettings.mysqluser,
      password: botSettings.mysqlpass,
      database: botSettings.mysqldb
    });

    var talkCooldown = 30000;
    var address = "";
    var author = message.author.id;

    if (talkedRecently.has(message.author.id)) {
        message.reply("Wait "+(talkCooldown/1000)+" seconds before playing again.");
    } else {

      if(message.channel.id === botChans.gamesroom) {

        function sendEgem(winnerAddress,finValue,prize){
          web3.eth.sendTransaction({
              from: botSettings.addressBot,
              to: winnerAddress,
              gas: web3.utils.toHex(miscSettings.txgas),
              value: web3.utils.toHex(finValue),
              data: web3.utils.toHex(miscSettings.messageSend)
          })
          .on('transactionHash', function(hash){

            const embed = new Discord.MessageEmbed()
            	.setColor(okcolor)
            	.setTitle("EGEM Discord Bot.")
            	.setURL(ghlink)
            	.setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })
            	.setDescription('Spin Rewards:')
            	.setThumbnail(img32shard)
            	.addFields(
            		{ name: 'User:', value: winnerAddress },
            		{ name: 'Amount:', value: prize+' EGEM ' },
            		{ name: 'Hash:', value: 'https://blockscout.egem.io/tx/' + hash},
            	)
            	.setTimestamp()
            	.setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })

            return message.reply({ embeds: [embed] });

          })
          .on('error', console.log);

        }

        con.connect(function(err) {
          if (err) {
            console.error('error connecting: ' + err.stack);
            return;
          }
          console.log('connected as id ' + con.threadId);
        });

        // Check if address is set
        
        con.query("SELECT addressCreds FROM userdata WHERE userId = ?", author, function (err, result){
          
          if(err) {
            con.end(function(err) {
              console.log('disconnected as id ' + con.threadId);
            })
            return message.reply('Error: Mysql Error.');
          } else if (!result.length) {   
            con.end(function(err) {
              console.log('disconnected as id ' + con.threadId);
            })                                                
            return message.reply('Error: No User found.');
          } else if (!result[0].addressCreds || result[0].addressCreds === "0") {
            con.end(function(err) {
              console.log('disconnected as id ' + con.threadId);
            })
            return message.reply('Error: No Address set in bot.');
          }

          address = result[0]['addressCreds'];
            
          // Variables
          var randBet = (Math.random() * (0.50 - 0.10)).toFixed(8);
          var bet = (Math.random() * (0.15 - 0.01) + Number(randBet)).toFixed(8);
          
          // Spin Results:
          var slotSymbols = [
            // x3 symbols
            ':star:',
            ':trophy:',
            ':slot_machine:',
            // x2 symbols
            ':hot_pepper:',
            ':gem:',
            ':candy:',
            // x1 symbols
            ':space_invader:',
            ':microbe:',
            ':house:'
          ];

          // Row 1:
          var rand1 = slotSymbols[(Math.random() * slotSymbols.length) | 0];
          var rand2 = slotSymbols[(Math.random() * slotSymbols.length) | 0];
          var rand3 = slotSymbols[(Math.random() * slotSymbols.length) | 0];
          var data1 = "";
          data1 += rand1 + " | " + rand2 + " | " + rand3;

          // Row 2:
          var rand4 = slotSymbols[(Math.random() * slotSymbols.length) | 0];
          var rand5 = slotSymbols[(Math.random() * slotSymbols.length) | 0];
          var rand6 = slotSymbols[(Math.random() * slotSymbols.length) | 0];
          var data2 = "";
          data2 += rand4 + " | " + rand5 + " | " + rand6;

          // Row 3:
          var rand7 = slotSymbols[(Math.random() * slotSymbols.length) | 0];
          var rand8 = slotSymbols[(Math.random() * slotSymbols.length) | 0];
          var rand9 = slotSymbols[(Math.random() * slotSymbols.length) | 0];
          var data3 = "";
          data3 += rand7 + " | " + rand8 + " | " + rand9;

          // Prize logic:
          // rand1 rand2 rand3
          // rand4 rand5 rand6
          // rand7 rand8 rand9

          // Prize List:
          // Each line multiplies the win.

          // Row wins & Empty Arrays:
          var rowWin = "";
          var winCount = "";
          var wonAmount = "";

          // Row wins:
          if (rand1 == ":slot_machine:" && rand2 == ":slot_machine:" && rand3 == ":slot_machine:") {rowWin += " :slot_machine: Row 1 was a winner!"; winCount += "xxx";}
          if (rand4 == ":slot_machine:" && rand5 == ":slot_machine:" && rand6 == ":slot_machine:") {rowWin += " :slot_machine: Row 2 was a winner!"; winCount += "xxx";}
          if (rand7 == ":slot_machine:" && rand8 == ":slot_machine:" && rand9 == ":slot_machine:") {rowWin += " :slot_machine: Row 3 was a winner!"; winCount += "xxx";}
          if (rand1 == ":trophy:" && rand2 == ":trophy:" && rand3 == ":trophy:") {rowWin += " :trophy: Row 1 was a winner!"; winCount += "xxx";}
          if (rand4 == ":trophy:" && rand5 == ":trophy:" && rand6 == ":trophy:") {rowWin += " :trophy: Row 2 was a winner!"; winCount += "xxx";}
          if (rand7 == ":trophy:" && rand8 == ":trophy:" && rand9 == ":trophy:") {rowWin += " :trophy: Row 3 was a winner!"; winCount += "xxx";}
          if (rand1 == ":star:" && rand2 == ":star:" && rand3 == ":star:") {rowWin += " :star: Row 1 was a winner!"; winCount += "xxx";}
          if (rand4 == ":star:" && rand5 == ":star:" && rand6 == ":star:") {rowWin += " :star: Row 2 was a winner!"; winCount += "xxx";}
          if (rand7 == ":star:" && rand8 == ":star:" && rand9 == ":star:") {rowWin += " :star: Row 3 was a winner!"; winCount += "xxx";}
          if (rand1 == ":hot_pepper:" && rand2 == ":hot_pepper:" && rand3 == ":hot_pepper:") {rowWin += " :hot_pepper: Row 1 was a winner!"; winCount += "xx";}
          if (rand4 == ":hot_pepper:" && rand5 == ":hot_pepper:" && rand6 == ":hot_pepper:") {rowWin += " :hot_pepper: Row 2 was a winner!"; winCount += "xx";}
          if (rand7 == ":hot_pepper:" && rand8 == ":hot_pepper:" && rand9 == ":hot_pepper:") {rowWin += " :hot_pepper: Row 3 was a winner!"; winCount += "xx";}
          if (rand1 == ":gem:" && rand2 == ":gem:" && rand3 == ":gem:") {rowWin += " :gem: Row 1 was a winner!"; winCount += "xx";}
          if (rand4 == ":gem:" && rand5 == ":gem:" && rand6 == ":gem:") {rowWin += " :gem: Row 2 was a winner!"; winCount += "xx";}
          if (rand7 == ":gem:" && rand8 == ":gem:" && rand9 == ":gem:") {rowWin += " :gem: Row 3 was a winner!"; winCount += "xx";}
          if (rand1 == ":candy:" && rand2 == ":candy:" && rand3 == ":candy:") {rowWin += " :candy: Row 1 was a winner!"; winCount += "xx";}
          if (rand4 == ":candy:" && rand5 == ":candy:" && rand6 == ":candy:") {rowWin += " :candy: Row 2 was a winner!"; winCount += "xx";}
          if (rand7 == ":candy:" && rand8 == ":candy:" && rand9 == ":candy:") {rowWin += " :candy: Row 3 was a winner!"; winCount += "xx";}
          if (rand1 == ":space_invader:" && rand2 == ":space_invader:" && rand3 == ":space_invader:") {rowWin += " :space_invader: Row 1 was a winner!"; winCount += "x";}
          if (rand4 == ":space_invader:" && rand5 == ":space_invader:" && rand6 == ":space_invader:") {rowWin += " :space_invader: Row 2 was a winner!"; winCount += "x";}
          if (rand7 == ":space_invader:" && rand8 == ":space_invader:" && rand9 == ":space_invader:") {rowWin += " :space_invader: Row 3 was a winner!"; winCount += "x";}
          if (rand1 == ":microbe:" && rand2 == ":microbe:" && rand3 == ":microbe:") {rowWin += " :microbe: Row 1 was a winner!"; winCount += "x";}
          if (rand4 == ":microbe:" && rand5 == ":microbe:" && rand6 == ":microbe:") {rowWin += " :microbe: Row 2 was a winner!"; winCount += "x";}
          if (rand7 == ":microbe:" && rand8 == ":microbe:" && rand9 == ":microbe:") {rowWin += " :microbe: Row 3 was a winner!"; winCount += "x";}
          if (rand1 == ":house:" && rand2 == ":house:" && rand3 == ":house:") {rowWin += " :house: Row 1 was a winner!"; winCount += "x";}
          if (rand4 == ":house:" && rand5 == ":house:" && rand6 == ":house:") {rowWin += " :house: Row 2 was a winner!"; winCount += "x";}
          if (rand7 == ":house:" && rand8 == ":house:" && rand9 == ":house:") {rowWin += " :house: Row 3 was a winner!"; winCount += "x";}

          // Vertical wins
          if (rand1 == ":slot_machine:" && rand4 == ":slot_machine:" && rand7 == ":slot_machine:") {rowWin += " :slot_machine: Reel 1 was a winner!"; winCount += "xxx";}
          if (rand2 == ":slot_machine:" && rand5 == ":slot_machine:" && rand8 == ":slot_machine:") {rowWin += " :slot_machine: Reel 2 was a winner!"; winCount += "xxx";}
          if (rand3 == ":slot_machine:" && rand6 == ":slot_machine:" && rand9 == ":slot_machine:") {rowWin += " :slot_machine: Reel 3 was a winner!"; winCount += "xxx";}
          if (rand1 == ":trophy:" && rand4 == ":trophy:" && rand7 == ":trophy:") {rowWin += " :trophy: Reel 1 was a winner!"; winCount += "xxx";}
          if (rand2 == ":trophy:" && rand5 == ":trophy:" && rand8 == ":trophy:") {rowWin += " :trophy: Reel 2 was a winner!"; winCount += "xxx";}
          if (rand3 == ":trophy:" && rand6 == ":trophy:" && rand9 == ":trophy:") {rowWin += " :trophy: Reel 3 was a winner!"; winCount += "xxx";}
          if (rand1 == ":star:" && rand4 == ":star:" && rand7 == ":star:") {rowWin += " :star: Reel 1 was a winner!"; winCount += "xxx";}
          if (rand2 == ":star:" && rand5 == ":star:" && rand8 == ":star:") {rowWin += " :star: Reel 2 was a winner!"; winCount += "xxx";}
          if (rand3 == ":star:" && rand6 == ":star:" && rand9 == ":star:") {rowWin += " :star: Reel 3 was a winner!"; winCount += "xxx";}
          if (rand1 == ":hot_pepper:" && rand4 == ":hot_pepper:" && rand7 == ":hot_pepper:") {rowWin += " :hot_pepper: Reel 1 was a winner!"; winCount += "xx";}
          if (rand2 == ":hot_pepper:" && rand5 == ":hot_pepper:" && rand8 == ":hot_pepper:") {rowWin += " :hot_pepper: Reel 2 was a winner!"; winCount += "xx";}
          if (rand3 == ":hot_pepper:" && rand6 == ":hot_pepper:" && rand9 == ":hot_pepper:") {rowWin += " :hot_pepper: Reel 3 was a winner!"; winCount += "xx";}
          if (rand1 == ":gem:" && rand4 == ":gem:" && rand7 == ":gem:") {rowWin += " :gem: Reel 1 was a winner!"; winCount += "xx";}
          if (rand2 == ":gem:" && rand5 == ":gem:" && rand8 == ":gem:") {rowWin += " :gem: Reel 2 was a winner!"; winCount += "xx";}
          if (rand3 == ":gem:" && rand6 == ":gem:" && rand9 == ":gem:") {rowWin += " :gem: Reel 3 was a winner!"; winCount += "xx";}
          if (rand1 == ":candy:" && rand4 == ":candy:" && rand7 == ":candy:") {rowWin += " :candy: Reel 1 was a winner!"; winCount += "xx";}
          if (rand2 == ":candy:" && rand5 == ":candy:" && rand8 == ":candy:") {rowWin += " :candy: Reel 2 was a winner!"; winCount += "xx";}
          if (rand3 == ":candy:" && rand6 == ":candy:" && rand9 == ":candy:") {rowWin += " :candy: Reel 3 was a winner!"; winCount += "xx";}
          if (rand1 == ":space_invader:" && rand4 == ":space_invader:" && rand7 == ":space_invader:") {rowWin += " :space_invader: Reel 1 was a winner!"; winCount += "x";}
          if (rand2 == ":space_invader:" && rand5 == ":space_invader:" && rand8 == ":space_invader:") {rowWin += " :space_invader: Reel 2 was a winner!"; winCount += "x";}
          if (rand3 == ":space_invader:" && rand6 == ":space_invader:" && rand9 == ":space_invader:") {rowWin += " :space_invader: Reel 3 was a winner!"; winCount += "x";}
          if (rand1 == ":microbe:" && rand4 == ":microbe:" && rand7 == ":microbe:") {rowWin += " :microbe: Reel 1 was a winner!"; winCount += "x";}
          if (rand2 == ":microbe:" && rand5 == ":microbe:" && rand8 == ":microbe:") {rowWin += " :microbe: Reel 2 was a winner!"; winCount += "x";}
          if (rand3 == ":microbe:" && rand6 == ":microbe:" && rand9 == ":microbe:") {rowWin += " :microbe: Reel 3 was a winner!"; winCount += "x";}
          if (rand1 == ":house:" && rand4 == ":house:" && rand7 == ":house:") {rowWin += " :house: Reel 1 was a winner!"; winCount += "x";}
          if (rand2 == ":house:" && rand5 == ":house:" && rand8 == ":house:") {rowWin += " :house: Reel 2 was a winner!"; winCount += "x";}
          if (rand3 == ":house:" && rand6 == ":house:" && rand9 == ":house:") {rowWin += " :house: Reel 3 was a winner!"; winCount += "x";}

          // Diagonal wins:
          if (rand1 == ":slot_machine:" && rand5 == ":slot_machine:" && rand9 == ":slot_machine:") {rowWin += " :slot_machine: Diagonal Win!"; winCount += "xxx";}
          if (rand3 == ":slot_machine:" && rand5 == ":slot_machine:" && rand7 == ":slot_machine:") {rowWin += " :slot_machine: Diagonal Win!"; winCount += "xxx";}
          if (rand1 == ":trophy:" && rand5 == ":trophy:" && rand9 == ":trophy:") {rowWin += " :trophy: Diagonal Win!"; winCount += "xxx";}
          if (rand3 == ":trophy:" && rand5 == ":trophy:" && rand7 == ":trophy:") {rowWin += " :trophy: Diagonal Win!"; winCount += "xxx";}
          if (rand1 == ":star:" && rand5 == ":star:" && rand9 == ":star:") {rowWin += " :star: Diagonal Win!"; winCount += "xxx";}
          if (rand3 == ":star:" && rand5 == ":star:" && rand7 == ":star:") {rowWin += " :star: Diagonal Win!"; winCount += "xxx";}
          if (rand1 == ":hot_pepper:" && rand5 == ":hot_pepper:" && rand9 == ":hot_pepper:") {rowWin += " :hot_pepper: Diagonal Win!"; winCount += "xx";}
          if (rand3 == ":hot_pepper:" && rand5 == ":hot_pepper:" && rand7 == ":hot_pepper:") {rowWin += " :hot_pepper: Diagonal Win!"; winCount += "xx";}
          if (rand1 == ":gem:" && rand5 == ":gem:" && rand9 == ":gem:") {rowWin += " :gem: Diagonal Win!"; winCount += "xx";}
          if (rand3 == ":gem:" && rand5 == ":gem:" && rand7 == ":gem:") {rowWin += " :gem: Diagonal Win!"; winCount += "xx";}
          if (rand1 == ":candy:" && rand5 == ":candy:" && rand9 == ":candy:") {rowWin += " :candy: Diagonal Win!"; winCount += "xx";}
          if (rand3 == ":candy:" && rand5 == ":candy:" && rand7 == ":candy:") {rowWin += " :candy: Diagonal Win!"; winCount += "xx";}
          if (rand1 == ":space_invader:" && rand5 == ":space_invader:" && rand9 == ":space_invader:") {rowWin += " :space_invader: Diagonal Win!"; winCount += "x";}
          if (rand3 == ":space_invader:" && rand5 == ":space_invader:" && rand7 == ":space_invader:") {rowWin += " :space_invader: Diagonal Win!"; winCount += "x";}
          if (rand1 == ":microbe:" && rand5 == ":microbe:" && rand9 == ":microbe:") {rowWin += " :microbe: Diagonal Win!"; winCount += "x";}
          if (rand3 == ":microbe:" && rand5 == ":microbe:" && rand7 == ":microbe:") {rowWin += " :microbe: Diagonal Win!"; winCount += "x";}
          if (rand1 == ":house:" && rand5 == ":house:" && rand9 == ":house:") {rowWin += " :house: Diagonal Win!"; winCount += "x";}
          if (rand3 == ":house:" && rand5 == ":house:" && rand7 == ":house:") {rowWin += " :house: Diagonal Win!"; winCount += "x";}

          // No wins:
          if (rowWin === "") {var rowWin = "No wins this spin, try again!";}

          // Prize Win logic
          var winsAmount = winCount.length;

          if (winCount === "") {
            var wonAmount = 0;
            var creditsWon = 0;
          } else {
            var wonAmount = (Number(bet) * Number(winsAmount));
            var creditsWon = (wonAmount*Math.pow(10,18));
          }

          let sql2 = "SELECT slotGame FROM usersystems WHERE id='1'";
          con.query(sql2, function (err, result) {
            if (!result) return message.reply("No results.");
            let parsed = JSON.stringify(result);
            let obj = JSON.parse(parsed);
            var slotGame = obj[0]['slotGame'];
            try {
              if (slotGame == "1") {
                return message.reply("Game Offline");
              } else {

                if (winsAmount == 0) {
                  console.log("Loss");
                  //

                  const embed = new Discord.MessageEmbed()
                    .setColor(okcolor)
                    .setTitle("EGEM Discord Bot.")
                    .setURL(ghlink)
                    .setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })
                    .setDescription('Spin Rewards:')
                    .setThumbnail(img32shard)
                    .addFields(
                      { name: 'Row #1:', value: `${data1}` },
                      { name: 'Row #2:', value: `${data2}` },
                      { name: 'Row #3:', value: `${data3}`},
                      { name: 'Results:', value: `${rowWin}`},
                    )
                    .setTimestamp()
                    .setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })

                  message.reply({ embeds: [embed] });

                } else {

                  var winnerAddress = address;
                  let weiAmount = bet*Math.pow(10,18);
                  let finValue = functions.numberToString(weiAmount);
                  console.log(winnerAddress,finValue);
                  console.log("Win");
                  console.log(wonAmount);
                  //
                  const embed = new Discord.MessageEmbed()
                    .setColor(okcolor)
                    .setTitle("EGEM Discord Bot.")
                    .setURL(ghlink)
                    .setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })
                    .setDescription('Spin Rewards:')
                    .setThumbnail(img32shard)
                    .addFields(
                      { name: 'Row #1:', value: `${data1}` },
                      { name: 'Row #2:', value: `${data2}` },
                      { name: 'Row #3:', value: `${data3}`},
                      { name: 'Results:', value: `${rowWin}`},
                      { name: 'Multiplier:', value: 'x ' + `${winsAmount}`},
                      { name: 'Amount Won:', value: `${Number(wonAmount).toFixed(8)}` + ' EGEM.'},
                    )
                    .setTimestamp()
                    .setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })

                  message.reply({ embeds: [embed] });

                  sendEgem(winnerAddress,finValue,wonAmount)

                }
                con.end(function(err) {
                  console.log('disconnected as id ' + con.threadId);
                });
                }
              } catch (e) {
                console.log(e)
                return message.reply("Error.");
              }
            })
        })

        

      } else {
        return message.reply("Use in games room.")
      }

        // Adds the user to the set so that they can't talk for 1 minute
        talkedRecently.add(message.author.id);
        setTimeout(() => {
          // Removes the user from the set after 1 minute
          talkedRecently.delete(message.author.id);
        }, talkCooldown);
    }

  }
}
