const { egemspin,okcolor,warningcolor,footerBranding,img32x32,img32shard,ghlink,web3IPC,web3HTTP,txgas,messageSend } = require('../../cfgs/settings.json');
const talkedRecently = new Set();

module.exports = {
  name: 'roll',
  description: "",
  async execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, mysql2, ipRegex, request, functions, mysql) {

    var con = mysql.createConnection({
      host: botSettings.mysqlip,
      user: botSettings.mysqluser,
      password: botSettings.mysqlpass,
      database: botSettings.mysqldb
    });

    var talkCooldown = 30000;

    if (talkedRecently.has(message.author.id)) {
        message.reply("Wait "+(talkCooldown/1000)+" seconds before playing again.");
    } else {

      if(message.channel.id === botChans.gamesroom) {

        function sendEgem(winnerAddress,finValue,prize){
          web3.eth.sendTransaction({
              from: botSettings.addressBot,
              to: winnerAddress,
              gas: web3.utils.toHex(miscSettings.txgas),
              value: web3.utils.toHex(finValue),
              data: web3.utils.toHex(miscSettings.messageSend)
          })
          .on('transactionHash', function(hash){

            const embed = new Discord.MessageEmbed()
              .setColor(okcolor)
              .setTitle("EGEM Discord Bot.")
              .setURL(ghlink)
              .setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })
              .setDescription('Spin Rewards:')
              .setThumbnail(img32shard)
              .addFields(
                { name: 'User:', value: winnerAddress },
                { name: 'Amount:', value: prize+' EGEM ' },
                { name: 'Hash:', value: 'https://blockscout.egem.io/tx/' + hash},
              )
              .setTimestamp()
              .setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })

            return message.reply({ embeds: [embed] });

          })
          .on('error', console.log);

        }

        // Variables
        var randBet = (Math.random() * (0.50 - 0.10)).toFixed(8);
        var bet = (Math.random() * (0.15 - 0.01) + Number(randBet)).toFixed(8);
        var author = message.author.id;
        var randNum = Math.floor(Math.random() * 12) + 1;

        // Dice Setup

        let die1 = Math.floor((Math.random() * 6) + 1);
        let die2 = Math.floor((Math.random() * 6) + 1);

        if (die1 == 6) {
          var dieSym1 = ":six:"
        } else if (die1 == 5) {
          var dieSym1 = ":five:"
        } else if (die1 == 4) {
          var dieSym1 = ":four:"
        } else if (die1 == 3) {
          var dieSym1 = ":three:"
        } else if (die1 == 2) {
          var dieSym1 = ":two:"
        } else {
          var dieSym1 = ":one:"
        }

        if (die2 == 6) {
          var dieSym2 = ":six:"
        } else if (die2 == 5) {
          var dieSym2 = ":five:"
        } else if (die2 == 4) {
          var dieSym2 = ":four:"
        } else if (die2 == 3) {
          var dieSym2 = ":three:"
        } else if (die2 == 2) {
          var dieSym2 = ":two:"
        } else {
          var dieSym2 = ":one:"
        }

        let roll = (die1 + die2);

        console.log(bet);
        console.log(randNum);
        console.log(roll);
        message.reply(dieSym1+" | "+dieSym2);


      } else {
        return message.reply("Use in games room.")
      }

        // Adds the user to the set so that they can't talk for 1 minute
        talkedRecently.add(message.author.id);
        setTimeout(() => {
          // Removes the user from the set after 1 minute
          talkedRecently.delete(message.author.id);
        }, talkCooldown);
    }

  }
}
