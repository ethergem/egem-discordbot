"use strict";

// Required
const mysql = require('mysql');

// Settings & Configs
const botSettings = require("./cfgs/config.json");
const miscSettings = require("./cfgs/settings.json");
const functions = require('./cfgs/functions');

var con = mysql.createPool({
  connectionLimit : 5,
  host: botSettings.mysqlip,
  user: botSettings.mysqluser,
  password: botSettings.mysqlpass,
  database: botSettings.mysqldb
});

// Add earned credits
async function creditsAdd(){
  con.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
          console.log("**PAY CREDIT STARTED** "+ functions.dateTime());
          con.query("SELECT tierOnePay15 FROM usersystems",function (err, result) {
            if (!result) return console.log("No Results.");
            var t1pay = result[0]['tierOnePay15'];
            console.log(t1pay);

            con.query("SELECT id, balance, mnrewards, address FROM userdata",function (err, result) {
              if (!result) return console.log("No Results.");
              let obj = JSON.stringify(result);
              let parsed = JSON.parse(obj);
              let userdata = result;

              function delay() {
                return new Promise(resolve => setTimeout(resolve, 20));
              }

              async function delayedLog(item) {
                await delay();
                try {
                  con.query("SELECT count FROM usernodes1 WHERE id =?",item.id ,function (err, result) {
                    try {
                      let count = result[0]['count']
                      let payZero = 0;
                      let earning = 1;
                      let tier1 = 1;

                      // Count online and pay.

                      let nodeCountPayable = Number(item.balance/miscSettings.collateral).toFixed(2);
                      let finalNodes = functions.toFixed(nodeCountPayable, 0)
                      let reqBal = (Number(miscSettings.collateral) * Number(count));
                      let payTierOne = Number(count * t1pay)

                      if (count > finalNodes) {
                        payTierOne = Number(finalNodes * t1pay);
                      }

                      if ( count > 0 ) {

                        // Update user data for payment.
                        let pay = Number(Number(item.mnrewards) + Number(payTierOne));
                        con.query(`UPDATE userdata SET mnrewards =? WHERE id = ?`, [functions.numberToString(pay),item.id]);
                        con.query(`UPDATE userdata SET userpay =? WHERE id = ?`, [functions.numberToString(payTierOne),item.id]);
                        con.query(`UPDATE userdata SET earning =? WHERE id = ?`, [earning,item.id]);
                        con.query(`UPDATE userdata SET tier =? WHERE id = ?`, [tier1,item.id]);

                        // Update lifetime total and last pay date.
                        con.query("UPDATE `userdata` SET `lifetimepay` = `lifetimepay` + "+(payTierOne/Math.pow(10,18))+" WHERE `id` = ?", [item.id]);
                        con.query(`UPDATE userdata SET lastpayment =? WHERE id = ?`, [new Date().toISOString().slice(0, 19).replace('T', ' '),item.id]);

                        console.log("Paying Node #: "+item.id+" | Chain Balance: "+Number(item.balance).toFixed(2)+" | Required Balance: "+reqBal+" | Node Count: "+count+" | Nodes Possible: "+finalNodes+" | Credit Balance: "+(item.mnrewards/Math.pow(10,18))+" | Pay: "+ (payTierOne/Math.pow(10,18))+" | New Credit Balance: "+ (pay/Math.pow(10,18)));
                        return;

                      } else {

                        // Set data to 0 if count less than 1 node.
                        con.query(`UPDATE userdata SET userpay =? WHERE id = ?`, [payZero,item.id]);
                        con.query(`UPDATE userdata SET earning =? WHERE id = ?`, [payZero,item.id]);
                        con.query(`UPDATE userdata SET tier =? WHERE id = ?`, [payZero,item.id]);
                        console.log("Not Paying Node #: "+item.id+" | Chain Balance: "+item.balance+" count: "+count);

                      }

                    } catch (e) {
                      console.log(e)
                    }
                  })
                } catch (e) {
                  console.log(e)
                }
              }
              async function processArray(array) {
                console.log("Add rewards started.");
                // Lock DB from other using it.
                await con.query(`UPDATE usersystems SET creditsUse = "1" WHERE id = "1"`);

                for (const item of array) {
                  await delayedLog(item);
                }

                // Unlock DB.
                await con.query(`UPDATE usersystems SET creditsUse = "0" WHERE id = "1"`);
                console.log("Add rewards complete "+ functions.dateTime());
              }
              processArray(userdata);
            })
          })
          con.releaseConnection(connection);
  });
};



console.log("**PAYING THREAD** is now Online.");

// Thread console heartbeat
const threadHB = function sendHB(){
	console.log("**PAYING THREAD** is ACTIVE");
};

setInterval(threadHB,120000);

var runTasks2 = function theTasks2() {
  creditsAdd();
}

setInterval(runTasks2,900000);
//setInterval(runTasks2,60000);
//creditsAdd();
