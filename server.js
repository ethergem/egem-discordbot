const fs = require('fs');
const Discord = require('discord.js');
const { token, prefix } = require('./cfgs/config.json');
const functions = require('./cfgs/functions.js');

var register = require(`./register`)
register.all();

var Intents = Discord.Intents;
var Collection = Discord.Collection;

const client = new Discord.Client({
  intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_PRESENCES, Intents.FLAGS.GUILD_MEMBERS, Intents.FLAGS.DIRECT_MESSAGES, Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILD_MESSAGE_REACTIONS],
  partials: ['MESSAGE', 'CHANNEL', 'REACTION'],
  allowedMentions: { parse: ['users', 'roles'], repliedUser: true },
});

client.commands = new Collection();
client.events = new Collection();
client.login(token);

// Event Handler
['event_handler', 'command_handler', 'interaction_handler'].forEach(handler => {
  require(`./handlers/${handler}`)(client, Discord)
});
