const { MessageEmbed } = require('discord.js');
const { SlashCommandBuilder } = require('@discordjs/builders');
const talkedRecentlyFaucet = new Map();

module.exports = {
	data: new SlashCommandBuilder()
		.setName('tipuser')
		.addStringOption(option => option.setName('amount').setDescription('Enter the amount of EGEM to send.').setRequired(true))
		.addStringOption(option => option.setName('user').setDescription('Mention a user.').setRequired(true))
		.setDescription('Send a small amount of EGEM to a registered user from your own credits!'),
	async execute(interaction, Discord, Duration, admins, botChans, botSettings, miscSettings, functions, web3, blacklist, mysql2) {

		var con = mysql2.createPool({
		  connectionLimit : 20,
		  host: botSettings.mysqlip,
		  user: botSettings.mysqluser,
		  password: botSettings.mysqlpass,
		  database: botSettings.mysqldb
		});

	  var userId = interaction.user.id;
	  //var person = interaction.mentions.users.first();
		var person = interaction.options.getString('user');
		let tippedAmmount = interaction.options.getString('amount');

	  //var userTipped = person.id;
		const removeEnd = person.slice(0, -1);
		var userTipped = removeEnd.replace( /^\D+/g, '');

		console.log(person);
		console.log(userTipped);
	  //var extra = args[2];

	  var maxAllowedBet = Number(110*Math.pow(10,18));
	  var minAllowedBet = Number(1000000000000);
	  var userTip = Number(tippedAmmount*Math.pow(10,18));

	  if (userTipped == userId) {
	    return interaction.reply("No self tipping allowed.");
	  }

	  var ifNeg = Math.sign(tippedAmmount);
	  if (ifNeg == "-1") {
	    return interaction.reply("You can't tip a negative amount, Abuse will result in a disabled account.");
	  }

	  if(isNaN(tippedAmmount)){
	   return interaction.reply("That is not a valid number, Abuse will result in a disabled account.");
	  }

	  if (userTip <= minAllowedBet) {
	    return interaction.reply("Im sorry you can not tip that amount! MIN 0.000001.");
	  }

	  if (userTip >= maxAllowedBet) {
	    return interaction.reply("Im sorry you can not tip that amount! MAX 100.");
	  }

	  // if (!userTipped || !tippedAmmount) {
	  //   return interaction.reply("Missing variables to send tip. '/tipuser discordid value'");
	  // }

	  // if (extra != null) {
	  //   return interaction.reply("No junk please, my owner feeds me enough.")
	  // }

	  con.getConnection(function(err, connection) {
	    if (err) throw err; // not connected!
	    let sql1 = "SELECT creditsUse FROM usersystems";
	    con.query(sql1, function (err, data, fields){
	      var inUse = data[0]['creditsUse'];
	      if (inUse == 1) {
	        return interaction.reply("Please try again in 1 minute, background actions being done currently.");
	      } else {
	            con.query("SELECT credits FROM userdata WHERE userId = ?", userTipped, function (err, result, fields){
	              try {

	                var tipUserCredits = result[0]['credits'];

	                con.query("SELECT credits FROM userdata WHERE userId = ?", userId, function (err, result1, fields){
	                  try {
	                    if (err) throw err; // not connected!
	                    var myCredits = result1[0]['credits'];
	                    var myCreditsNum  = result1[0]['credits']/Math.pow(10,18);

	                    if (tippedAmmount > myCreditsNum) {
	                      return interaction.reply("Not enough to send tip.");
	                    } else {
	                      var leftOverCredits = (Number(myCredits) - Number(tippedAmmount*Math.pow(10,18)));
	                      var finalXfer = (Number(tipUserCredits) + Number(tippedAmmount*Math.pow(10,18)));

	                      con.query(`UPDATE userdata SET credits = ? WHERE userId = ?`, [functions.numberToString(leftOverCredits), userId]);
	                      con.query(`UPDATE userdata SET credits = ? WHERE userId = ?`, [functions.numberToString(finalXfer), userTipped]);

	                      console.log(functions.numberToString(leftOverCredits));
	                      console.log(functions.numberToString(finalXfer));

	                      const embed = new Discord.MessageEmbed()
	                        .setTitle("Result:")
	                        .setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })

	                        .setColor(miscSettings.okcolor)
	                        .setDescription("Tip User:")
	                        .setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })
	                        .setThumbnail(miscSettings.img32shard)

	                        .setTimestamp()
	                        .setURL(miscSettings.ghlink)
	                        .addField("You have tipped EGEM: ", tippedAmmount)

	                        return interaction.reply({ embeds: [embed] })
	                    }
	                  } catch (e) {
	                    console.log(e)
	                    interaction.reply("User Not registered, send our bot a message with /register.");
	                  }
	                })
	              } catch (e) {
	                console.log(e)
	                return interaction.reply("User lookup error check discord id.");
	              }
	            })
	          con.releaseConnection(connection);
	      }
	    })
	  })

	},
};
