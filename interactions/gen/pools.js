const Web3 = require("web3");
const { MessageEmbed } = require('discord.js');
const { egemspin,okcolor,footerBranding,img32x32,img32shard,ghlink,web3IPC,web3HTTP } = require('../../cfgs/settings.json');
const { mysqlip, mysqluser,mysqlpass,mysqldb,addressBot } = require('../../cfgs/config.json');
const { SlashCommandBuilder } = require('@discordjs/builders');

// EtherGem web3
var net = require('net');
//local
var web3 = new Web3(new Web3.providers.IpcProvider(web3IPC, net));

module.exports = {
	data: new SlashCommandBuilder()
		.setName('pools')
		.setDescription('List of current and online pools!'),
	async execute(interaction) {

		const embed = new MessageEmbed()
			.setTitle("Result:")
			.setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })

			.setColor(okcolor)
			.setDescription("Pool list:")
			.setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })
			.setThumbnail(img32shard)

			.setTimestamp()
			.setURL(ghlink)
			.addField("Digipools.org","https://egem.digipools.org/")
			.addField("Comining.io","https://comining.io/")
			.addField("Zergpool.com","http://zergpool.com/")
			.addField("Wattpool.net","https://ethergem.wattpool.net/")
			.addField("Coolpool.top (SOLO)","https://coolpool.top/pool?coin=egemSolo&type=solo")
			.addField("Wattpool.net (SOLO)","https://ethergem-solo.wattpool.net/")

		interaction.reply({ embeds: [embed] })

	},
};
