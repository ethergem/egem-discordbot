const Web3 = require("web3");
const { MessageEmbed } = require('discord.js');
const { egemspin,okcolor,footerBranding,img32x32,img32shard,ghlink,web3IPC,web3HTTP } = require('../../cfgs/settings.json');
const { mysqlip, mysqluser,mysqlpass,mysqldb,addressBot } = require('../../cfgs/config.json');
const { SlashCommandBuilder } = require('@discordjs/builders');

// EtherGem web3
var net = require('net');
//local
var web3 = new Web3(new Web3.providers.IpcProvider(web3IPC, net));

var mysql = require('mysql2');

var con = mysql.createPool({
  connectionLimit : 20,
  host: mysqlip,
  user: mysqluser,
  password: mysqlpass,
  database: mysqldb
});

module.exports = {
	data: new SlashCommandBuilder()
		.setName('paystats')
		.setDescription('Replies with current status of node pay!'),
	async execute(interaction) {

    var userId = interaction.member.id;

    con.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
      try {
        con.query("SELECT avgusd,tierOnePay15,tierTwoPay15,roundPay FROM usersystems WHERE id = '1'", function (err, data, fields){
          if (err) throw err; // not connected!

          var avgusd = Number(data[0]['avgusd']);
          var roundPay = Number(data[0]['roundPay']/Math.pow(10,18));

          var tierOne = 10000;
          var tierOnePay15 = (data[0]['tierOnePay15']/Math.pow(10,18));
          var tierOnePay15Hourly = (tierOnePay15 * 4);
          var tierOnePay15Daily = (tierOnePay15Hourly * 24);
          var tierOnePay15Monthly = (tierOnePay15Daily * 30.41666666666667);
          var tierOnePay15Yearly = (tierOnePay15Monthly * 12);
          var nodeOneROI = (tierOnePay15Yearly) / (avgusd * tierOne);
          var nodeOneHourlyROI = (tierOnePay15Hourly / tierOne) * 100;
          var nodeOneDailyROI = (tierOnePay15Daily / tierOne) * 100;
          var nodeOneMonthlyROI = (tierOnePay15Monthly / tierOne) * 100;
          var nodeOneYearlyROI = (tierOnePay15Yearly / tierOne) * 100;

          // var tierTwo = 40000;
          // var tierTwoPay15 = (data[0]['tierTwoPay15']/Math.pow(10,18));
          // var tierTwoPay15Hourly = (tierTwoPay15 * 4);
          // var tierTwoPay15Daily = (tierTwoPay15Hourly * 24);
          // var tierTwoPay15Monthly = (tierTwoPay15Daily * 30.41666666666667);
          // var tierTwoPay15Yearly = (tierTwoPay15Monthly * 12);
          // var nodeTwoHourlyROI = (tierTwoPay15Hourly / tierTwo) * 100;
          // var nodeTwoDailyROI = (tierTwoPay15Daily / tierTwo) * 100;
          // var nodeTwoMonthlyROI = (tierTwoPay15Monthly / tierTwo) * 100;
          // var nodeTwoYearlyROI = (tierTwoPay15Yearly / tierTwo) * 100;


          try {
            if (err) throw err; // not connected!

            const embed = new MessageEmbed()
              .setTitle("Result:")
              .setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })

              .setColor(okcolor)
              .setDescription("Pay Stats:")
              .setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })
              .setThumbnail(img32shard)

              .setTimestamp()
              .setURL(ghlink)
              .addField("T1 Hourly",Number(tierOnePay15Hourly).toFixed(4)+" EGEM | $"+Number(tierOnePay15Hourly*avgusd).toFixed(4)+" USD | "+ (nodeOneHourlyROI).toFixed(4)+"% ROI")
              .addField("T1 Daily",Number(tierOnePay15Daily).toFixed(4)+" EGEM | $"+Number(tierOnePay15Daily*avgusd).toFixed(4)+" USD | "+ (nodeOneDailyROI).toFixed(4)+"% ROI")
              .addField("T1 Monthly",Number(tierOnePay15Monthly).toFixed(4)+" EGEM | $"+Number(tierOnePay15Monthly*avgusd).toFixed(4)+" USD | "+ (nodeOneMonthlyROI).toFixed(4)+"% ROI")
              .addField("T1 Yearly",Number(tierOnePay15Yearly).toFixed(4)+" EGEM | $"+Number(tierOnePay15Yearly*avgusd).toFixed(4)+" USD | "+ (nodeOneYearlyROI).toFixed(4)+"% ROI")

            interaction.reply({ embeds: [embed] })

          } catch (e) {
            console.log(e)
          }
        })
      } catch (e) {
        console.log(e)
      }
      con.releaseConnection(connection);
    })
	},
};

// .addField("T2 Hourly",Number(tierTwoPay15Hourly).toFixed(4)+" EGEM | $"+Number(tierTwoPay15Hourly*avgusd).toFixed(4)+" USD | "+ (nodeTwoHourlyROI).toFixed(4)+"% ROI")
// .addField("T2 Daily",Number(tierTwoPay15Daily).toFixed(4)+" EGEM | $"+Number(tierTwoPay15Daily*avgusd).toFixed(4)+" USD | "+ (nodeTwoDailyROI).toFixed(4)+"% ROI")
// .addField("T2 Monthly",Number(tierTwoPay15Monthly).toFixed(4)+" EGEM | $"+Number(tierTwoPay15Monthly*avgusd).toFixed(4)+" USD | "+ (nodeTwoMonthlyROI).toFixed(4)+"% ROI")
// .addField("T2 Yearly",Number(tierTwoPay15Yearly).toFixed(4)+" EGEM | $"+Number(tierTwoPay15Yearly*avgusd).toFixed(4)+" USD | "+ (nodeTwoYearlyROI).toFixed(4)+"% ROI")
