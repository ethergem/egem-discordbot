const Web3 = require("web3");
const { MessageEmbed } = require('discord.js');
const { egemspin,okcolor,footerBranding,img32x32,img32shard,ghlink,web3IPC,web3HTTP } = require('../../cfgs/settings.json');
const { mysqlip, mysqluser,mysqlpass,mysqldb,addressBot } = require('../../cfgs/config.json');
const { SlashCommandBuilder } = require('@discordjs/builders');

// EtherGem web3
var net = require('net');
//local
var web3 = new Web3(new Web3.providers.IpcProvider(web3IPC, net));

var mysql = require('mysql2');

var con = mysql.createPool({
  connectionLimit : 20,
  host: mysqlip,
  user: mysqluser,
  password: mysqlpass,
  database: mysqldb
});

module.exports = {
	data: new SlashCommandBuilder()
		.setName('botstats')
		.setDescription('Replies with current status of our bot!'),
	async execute(interaction) {

    var userId = interaction.member.id;

    con.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
      try {
        con.query("SELECT currentSupply,countedBotCoins,totalCredits,totalMNRewards,totalUsers,totalNodes,usersEarning FROM usersystems WHERE id = '1'", userId, function (err, data, fields){
          if (err) throw err; // not connected!

          var currentSupply = Number(data[0]['currentSupply']);
          var countedBotCoins = Number(data[0]['countedBotCoins']);
          var totalTipCredits = (data[0]['totalCredits']/Math.pow(10,18));
          var totalMNRewards = (data[0]['totalMNRewards']/Math.pow(10,18));
          var totalUsers = data[0]['totalUsers'];
          var totalNodes = data[0]['totalNodes'];
          var usersEarning = data[0]['usersEarning'];
          var totalBalance = (totalTipCredits + totalMNRewards).toFixed(2);
          var supplyPercent = ((countedBotCoins / currentSupply) * 100).toFixed(2);

          web3.eth.getBalance(addressBot, "latest")
          .then(function(result){

            var avaliableBalance = (result/Math.pow(10,18)).toFixed(2);
            var percentLeft = ((totalBalance - avaliableBalance) / avaliableBalance).toFixed(2);

            try {
              if (err) throw err; // not connected!

              const embed = new MessageEmbed()
                .setTitle("Result:")
                .setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })

                .setColor(okcolor)
                .setDescription("Bot Stats:")
                .setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })
                .setThumbnail(img32shard)

                .setTimestamp()
                .setURL(ghlink)
                .addField("Coins in Accounts:",(countedBotCoins).toFixed(0)+"/"+currentSupply+" EGEM "+supplyPercent+"%")
                .addField("Tip Credits:",new Intl.NumberFormat().format(totalTipCredits)+" EGEM")
                .addField("MN Credits:",new Intl.NumberFormat().format(totalMNRewards)+" EGEM")
                .addField("Funds Avaliable:",totalBalance+"/"+avaliableBalance+" Percent Left: "+(percentLeft * 100)+"%")
                .addField("Total Users:",totalUsers)
                .addField("Total Nodes:",totalNodes)
                .addField("Earning Users:",usersEarning)


              interaction.reply({ embeds: [embed] })
            } catch (e) {
              console.log(e)
            }
          });

        })
      } catch (e) {
        console.log(e)
      }
      con.releaseConnection(connection);
    })

	},
};
