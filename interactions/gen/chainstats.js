const Web3 = require("web3");
const { MessageEmbed } = require('discord.js');
const { egemspin,okcolor,footerBranding,img32x32,img32shard,ghlink,web3IPC,web3HTTP } = require('../../cfgs/settings.json');
const { mysqlip, mysqluser,mysqlpass,mysqldb,addressBot } = require('../../cfgs/config.json');
const { SlashCommandBuilder } = require('@discordjs/builders');

// EtherGem web3
var net = require('net');
//local
var web3 = new Web3(new Web3.providers.IpcProvider(web3IPC, net));

var mysql = require('mysql2');

var con = mysql.createPool({
  connectionLimit : 20,
  host: mysqlip,
  user: mysqluser,
  password: mysqlpass,
  database: mysqldb
});

module.exports = {
	data: new SlashCommandBuilder()
		.setName('chainstats')
		.setDescription('Replies with current status of the chain.'),
	async execute(interaction) {

    var userId = interaction.member.id;

    con.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
      try {
        con.query("SELECT avgusd,avgbtc,currentBlock,currentSupply,currentMcap FROM usersystems WHERE id = '1'", userId, function (err, data, fields){
          if (err) throw err; // not connected!
          var avgusd = data[0]['avgusd'];
          var avgbtc = data[0]['avgbtc'];
          var currentBlock = data[0]['currentBlock'];
          var currentSupply = data[0]['currentSupply'];
          var currentMcap = data[0]['currentMcap'];

          try {
            if (err) throw err; // not connected!

            const embed = new MessageEmbed()
              .setTitle("Result:")
              .setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })

              .setColor(okcolor)
              .setDescription("Chain Stats:")
              .setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })
              .setThumbnail(img32shard)

              .setTimestamp()
              .setURL(ghlink)
              .addField("Block:",new Intl.NumberFormat().format(currentBlock))
              .addField("Supply:",new Intl.NumberFormat().format(currentSupply)+" EGEM")
              .addField("MCap:","$ "+new Intl.NumberFormat().format(currentMcap))
              .addField("Price:","$ "+Number(avgusd).toFixed(5))
              .addField("Price:",avgbtc+" BTC")

            interaction.reply({ embeds: [embed] })
          } catch (e) {
            console.log(e)
          }
        })
      } catch (e) {
        console.log(e)
      }
      con.releaseConnection(connection);
    })

	},
};
