const Web3 = require("web3");
const { MessageEmbed } = require('discord.js');
const { egemspin,okcolor,footerBranding,img32x32,img32shard,ghlink,web3IPC,web3HTTP } = require('../../cfgs/settings.json');
const { mysqlip, mysqluser,mysqlpass,mysqldb,addressBot } = require('../../cfgs/config.json');
const { SlashCommandBuilder } = require('@discordjs/builders');

// EtherGem web3
var net = require('net');
//local
var web3 = new Web3(new Web3.providers.IpcProvider(web3IPC, net));

module.exports = {
	data: new SlashCommandBuilder()
		.setName('getbal')
    .addStringOption(option => option.setName('address').setDescription('Enter a address to look a balance.').setRequired(true))
		.setDescription('Replies with current balance of a address!'),
	async execute(interaction) {

    const userId = interaction.member.id;
    const address = interaction.options.getString('address');

    web3.eth.getBalance(address, "latest")
    .then(function(result2){
      var egembal = result2/Math.pow(10,18);
      interaction.reply("That address contains: " + egembal + " EGEM.")
    });

	},
};
