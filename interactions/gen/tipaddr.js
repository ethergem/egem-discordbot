const { MessageEmbed } = require('discord.js');
const { SlashCommandBuilder } = require('@discordjs/builders');
const talkedRecentlyFaucet = new Map();

module.exports = {
	data: new SlashCommandBuilder()
		.setName('tipaddr')
    .addStringOption(option => option.setName('address').setDescription('Enter a address to send a tip in EGEM to.').setRequired(true))
		.addStringOption(option => option.setName('amount').setDescription('Enter the amount of EGEM to send.').setRequired(true))
		.setDescription('Send a small amount of EGEM to a address!'),
	async execute(interaction, Discord, Duration, admins, botChans, botSettings, miscSettings, functions, web3, blacklist, mysql2) {

		var con = mysql2.createPool({
		  connectionLimit : 20,
		  host: botSettings.mysqlip,
		  user: botSettings.mysqluser,
		  password: botSettings.mysqlpass,
		  database: botSettings.mysqldb
		});

		con.getConnection(function(err, connection) {
			if (err) throw err; // not connected!
			try {
				con.query("SELECT tipFunds FROM usersystems WHERE id = '1'", function (err, data, fields){
					if (err) throw err; // not connected!

					let userId = interaction.user.id;
			    let userAddress = interaction.options.getString('address');
					let amount = interaction.options.getString('amount');

					let avlFunds = Number(data[0]["tipFunds"]/Math.pow(10,18));
					let addycheck = web3.utils.isAddress(userAddress);

					let cooldown = talkedRecentlyFaucet.get(interaction.user.id);

					let myId = 1;
					let maxSend = 100;
					let minSend = 0.01;

					// check for a valid address
					if (addycheck !== true) {
						return interaction.reply("Not a valid EGEM address.")
					}
					// make sure amount sending is a valid number.
					if (isNaN(amount)) {
		        return interaction.reply("Not a number.")
		      }
					// if less than min reject
					if (amount < minSend) {
						return interaction.reply("You need to send a minimum of "+minSend)
					}
					// set max send and make sure enough funds and amount < avlFunds
					if(amount > avlFunds || amount > maxSend || avlFunds === 0) {
						return interaction.reply("Amount to high (100 Max) or not enough in bots tip bank to send, the bot currently holds "+ avlFunds +" EGEM. Use the tip deposit command to top up the funds.")
					}

					console.log(avlFunds);
					console.log(userAddress);
					console.log(amount);

					if (cooldown) {
						const remaining = Duration(cooldown - Date.now(), { units: ['h', 'm'], round: true})

						interaction.reply("Cooldown trigger, time remaining: "+remaining);
					} else {
						//interaction.reply("Check logs");

						// set cooldown
						if (admins.includes(interaction.user.id)) {
							console.log("Admin used.");
						} else {
							talkedRecentlyFaucet.set(interaction.user.id, Date.now() + 1000 * 60 * 60 * 4);
							setTimeout(() => { talkedRecentlyFaucet.delete(interaction.user.id)}, 1000 * 60 * 60 * 4);
						}

						// update bots tip funds prior to sending
						let finTipFunds = Number(avlFunds - amount)*Math.pow(10,18);
						console.log(finTipFunds);
						con.query("UPDATE usersystems SET tipFunds = ? WHERE id = ?", [finTipFunds, myId]);

						// trigger coin send
						let finValue = Number(amount*Math.pow(10,18));
						//console.log(functions.numberToString(finValue));
						sendCoins(userAddress, finValue)
					}

					// Send coins functions
					function sendCoins(userAddress, finValue) {
						web3.eth.sendTransaction({
								from: botSettings.addressBot,
								to: userAddress,
								gas: web3.utils.toHex(miscSettings.txgas),
								value: web3.utils.toHex(finValue),
								data: web3.utils.toHex(miscSettings.messageSend)
						})
						.on('transactionHash', function(hash){

							const embed = new Discord.MessageEmbed()
								.setTitle("EGEM Discord Bot.")
								.setAuthor({ name: 'TheEGEMBot', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/animated/egem_gray_28.gif', url: 'https://gitlab.com/ethergem/egem-discordbot' })

								.setColor(miscSettings.okcolor)
								.setDescription("Tip:")
								.setFooter({ text: '© EGEM.io', iconURL: 'https://gitlab.com/ethergem/egem-meta/raw/master/images/32x32.png' })
								.setThumbnail(miscSettings.img32shard)

								.setTimestamp()
								.setURL(miscSettings.ghlink)
								.addField("User:",userAddress)
								.addField("Amount:", amount+" EGEM")
								.addField("Tip Hash:","https://blockscout.egem.io/tx/" + hash)

								return interaction.reply({ embeds: [embed] })
						})
						.on('error', console.log);
					}

				});
				con.releaseConnection(connection);
			} catch (e) {
				console.log(e);
			}

		});
	},
};
