// Admins
const admins = require('../../cfgs/discordids/admins.json');

// Channels & Ids
const botChans = require('../../cfgs/discordids/botchans.json');
const blacklist = require("../../cfgs/discordids/addressBlacklist.json");

// Configs
const botSettings = require('../../cfgs/config.json');
const miscSettings = require('../../cfgs/settings.json');
const functions = require('../../cfgs/functions.js');

// Packages
const Web3 = require("web3");
const net = require('net');
const mysql2 = require('mysql2');
const ipRegex = require('ip-regex');
const request = require ("request");;
const Duration = require('humanize-duration');

// Web3 portals
var web3 = new Web3(new Web3.providers.IpcProvider(miscSettings.web3IPC, net));
var web3HTTP = new Web3(new Web3.providers.HttpProvider(miscSettings.web3HTTP));

module.exports = (client, Discord, interaction) => {
    //console.log(interaction);
    if (!interaction.isCommand()) return;

    const command = client.commands.get(interaction.commandName);

  	if (!command) return;

    if(command){
      command.execute(interaction, Discord, Duration, admins, botChans, botSettings, miscSettings, functions, web3, blacklist, mysql2);
      client.channels.cache.get(botChans.cmdlogs).send("Interaction Used: USER: <@"+ interaction.user.id+"> Command: "+interaction.commandName);
    } else {
      interaction.reply({ content: 'Bot command failure, check the logs!', ephemeral: true });
    }

};
