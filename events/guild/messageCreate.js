// Word Filter
const badWords = require('../../cfgs/wordfilters/badWords');
const sexualWords = require('../../cfgs/wordfilters/sexualWords');
const funnyWords = require('../../cfgs/wordfilters/funnyWords');
const cheersWords = require('../../cfgs/wordfilters/cheersWords');

// Reply Lists
const sexualSayings = require('../../cfgs/wordreplys/sexualSayings');
const sarcSayings = require('../../cfgs/wordreplys/sarcSayings');

// Reaction List
const badReact = require('../../cfgs/wordreacts/badReact');
const sexReact = require('../../cfgs/wordreacts/sexReact');
const funReact = require('../../cfgs/wordreacts/sexReact');
const cheersReact = require('../../cfgs/wordreacts/sexReact');

// Admins
const admins = require('../../cfgs/discordids/admins.json');

// Channels
const botChans = require('../../cfgs/discordids/botchans.json');

// Roles
const userRoles = require('../../cfgs/discordids/roles.json');

// Configs
const botSettings = require('../../cfgs/config.json');
const miscSettings = require('../../cfgs/settings.json');
const functions = require('../../cfgs/functions.js');
const blacklist = require("../../cfgs/discordids/addressBlacklist.json");


// Packages
const Web3 = require("web3");
const net = require('net');
const mysql = require('mysql');
const mysql2 = require('mysql2');
const ipRegex = require('ip-regex');
const request = require ("request");
const Duration = require('humanize-duration');

// Web3 portals
var web3 = new Web3(new Web3.providers.IpcProvider(miscSettings.web3IPC, net));
var web3R = new Web3(new Web3.providers.HttpProvider(miscSettings.web3HTTP));

module.exports = (client, Discord, message) => {
  const prefix = '/';
  const args = message.content.slice(prefix.length).trim().split(/ +/);
  const cmd = args.shift().toLowerCase();
  const command = client.commands.get(cmd);

  var chanId = message.channel.id;
  var chanArray = [
    "409528861353574401"
  ]

  if (command) {
    // Execute Command
    command.execute(client, message, args, Discord, admins, botChans, botSettings, miscSettings, web3, mysql2, ipRegex, request, functions, mysql, web3R, userRoles, Duration, blacklist)
    client.channels.cache.get(botChans.cmdlogs).send("Command Used: USER: <@"+ message.author.id+"> Command: "+cmd+ " | Args: "+args);
  } else {
    // Check if blacklisted channel
    if(!chanArray.includes(chanId)) return;

    // Check message and respond accordingly.
    if(message.author.bot) return;

    // Check message content for bad/sexual/funny/cheers words and react/reply.
    var author = message.author.id;
    var msg2Check = message.content;

    // Pick rand reaction
    var randBad = badReact[(Math.random() * badReact.length) | 0];
    var randSexual = sexReact[(Math.random() * sexReact.length) | 0];
    var randFunny = funReact[(Math.random() * funReact.length) | 0];
    var randCheers = cheersReact[(Math.random() * cheersReact.length) | 0];

    // Reply from list
    var replySarc = sarcSayings[(Math.random() * sarcSayings.length) | 0];
    var replySexual = sexualSayings[(Math.random() * sexualSayings.length) | 0];

    // Reply with emoji reaction
    function msgReact(client,content,react) {
      message.react(react);
      client.channels.cache.get(botChans.faillogs).send("USER: <@"+ message.author.id+"> Triggered word filter with: "+content);
    }

    //Reply with message response
    function msgReply(client,content,saying) {
      message.reply(saying);
      client.channels.cache.get(botChans.faillogs).send("USER: <@"+ message.author.id+"> Triggered word filter with: "+content);
    }

    // Check content of messages
    if (args.length > 1) {
      // Check whole message
      for (var i = 0; i < args.length; i++) {
        if ( badWords.includes(args[i]) ) {
          message.react(randBad);
          return msgReply(client, msg2Check, replySarc);
        }
        if ( sexualWords.includes(args[i]) ) {
          message.react(randSexual);
          return msgReply(client, msg2Check, replySexual);
        }
        if ( funnyWords.includes(args[i]) ) {
          return msgReply(client, msg2Check, randFunny);
        }
        if ( cheersWords.includes(args[i]) ) {
          return msgReply(client, msg2Check, randCheers);
        }
      }
    } else {
      // Check first word if only one sent.
      if ( badWords.includes(msg2Check) ) {
        return msgReact(client, msg2Check, randBad);
      }
      if ( sexualWords.includes(msg2Check) ) {
        return msgReact(client, msg2Check, randSexual);
      }
      if ( funnyWords.includes(msg2Check) ) {
        return msgReact(client, msg2Check, randFunny);
      }
      if ( cheersWords.includes(msg2Check) ) {
        return msgReact(client, msg2Check, randCheers);
      }
    }
    // End
  }

};
