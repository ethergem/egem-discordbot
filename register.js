const { REST } = require('@discordjs/rest');
const { Routes } = require('discord-api-types/v9');
const { token } = require('./cfgs/config.json');
const fs = require('fs');

const commands = [];
const commandsPm = [];

const commandFiles = fs.readdirSync('./interactions/gen').filter(file => file.endsWith('.js'));

// Place your client and guild ids here
const clientId = '630817850843725874';
const guildId = '818440388381245471';

var all = function all(){

	for (const file of commandFiles) {
		const command = require(`./interactions/gen/${file}`);
		commands.push(command.data.toJSON());
	}

	const rest = new REST({ version: '9' }).setToken(token);

	(async () => {
		try {
			console.log('Started refreshing egem (/) commands.');

			await rest.put(
				Routes.applicationGuildCommands(clientId, guildId),
				{ body: commands },
			);

			await rest.put(
				Routes.applicationCommands(clientId),
				{ body: commandsPm },
			);

			console.log('Successfully reloaded egem (/) commands.');
		} catch (error) {
			console.error(error);
		}
	})();
};

module.exports.all = all;
