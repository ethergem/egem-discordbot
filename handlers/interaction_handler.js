const fs = require('fs');

module.exports = (client, Discord) => {
  const load_dir = (dirs) => {
    const commandFiles = fs.readdirSync(`./interactions/${dirs}`).filter(file => file.endsWith('.js'));

    for (const file of commandFiles) {
      const command = require(`../interactions/${dirs}/${file}`);
      client.commands.set(command.data.name, command);
    }
  }
  ['gen'].forEach(e => load_dir(e));
}
