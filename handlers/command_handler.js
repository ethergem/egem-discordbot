const fs = require('fs');

module.exports = (client, Discord) => {
  const load_dir = (dirs) => {
    const commandFiles = fs.readdirSync(`./commands/${dirs}`).filter(file => file.endsWith('.js'));

    for (const file of commandFiles) {
      const command = require(`../commands/${dirs}/${file}`);
      if (command.name) {
        client.commands.set(command.name, command);
      } else {
        continue;
      }
    }
  }
  ['admin', 'private', 'games', 'gen', 'help'].forEach(e => load_dir(e));
}
