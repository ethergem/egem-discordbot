const fs = require('fs');

module.exports = (client, Discord) => {
  const load_dir = (dirs) => {
    const eventFiles = fs.readdirSync(`./events/${dirs}`).filter(file => file.endsWith('.js'));

    for (const file of eventFiles) {
      const event = require(`../events/${dirs}/${file}`);
      const event_name = file.split('.')[0];
      //client.on(event_name, event.bind(null, client, Discord));
    	if (event.once) {
    		client.once(event_name, (...args) => event.execute(...args));
    	} else {
        client.on(event_name, event.bind(null, client, Discord));
    		//client.on(event_name, (...args) => event.execute(...args));
    	}
    }
  }
  ['client', 'guild'].forEach(e => load_dir(e));
}
