// Required
const mysql = require('mysql2');
const request = require ("request");
const getJSON = require('get-json');
const net = require('net');

// Configs
const botSettings = require('../cfgs/config.json');
const miscSettings = require('../cfgs/settings.json');
const functions = require('../cfgs/functions.js');

const nodeCount = 11; // 10 +1 fake

// Create Main SQL Pool
const con = mysql.createPool({
  connectionLimit : 50,
  host: botSettings.mysqlip,
  user: botSettings.mysqluser,
  password: botSettings.mysqlpass,
  database: botSettings.mysqldb
});

// Functions

con.getConnection(function(err, connection) {
    if (err) throw err; // not connected!

        console.log("**CLEANING STARTED** "+ functions.dateTime());
        function delay() {
          return new Promise(resolve => setTimeout(resolve, 50));
        }

        async function delayedLog(item) {
          await delay();
          // do stuff
          let zero = 0;
          console.log(item);
          con.query("UPDATE `userdata` SET `lifetimepay` = ? WHERE `id` = ?", [zero,item.id]);

        }

        async function processArray(array) {
          for (const item of array) {
            await delayedLog(item);
          }
          console.log("**DONE** "+ functions.dateTime());
        }

        try {
          con.query("SELECT id,mnrewards,credits,lifetimepay FROM userdata", function (err, result) {
            if(err){console.log(err);}
            //console.log(result);
            let parsed = JSON.stringify(result);
            let obj = JSON.parse(parsed);
            processArray(obj);
          })
        } catch (e) {
          console.log(e)
        }
        con.releaseConnection(connection);

});
