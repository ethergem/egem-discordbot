// Required
const mysql = require('mysql2');
const request = require ("request");
const getJSON = require('get-json');
const net = require('net');

// Configs
const botSettings = require('../cfgs/config.json');
const miscSettings = require('../cfgs/settings.json');
const functions = require('../cfgs/functions.js');

const nodeCount = 11; // 10 +1 fake

// Create Main SQL Pool
const con = mysql.createPool({
  connectionLimit : 50,
  host: botSettings.mysqlip,
  user: botSettings.mysqluser,
  password: botSettings.mysqlpass,
  database: botSettings.mysqldb
});

// Functions

con.getConnection(function(err, connection) {
    if (err) throw err; // not connected!

        console.log("**NODE MIGRATION STARTED** "+ functions.dateTime());
        function delay() {
          return new Promise(resolve => setTimeout(resolve, 150));
        }

        async function delayedLog(item) {
          await delay();
          // do stuff

          for (i = 1; i < nodeCount; i++) {
            let node = item['n'+i];
            let slot = Number(10 + i);

            console.log(item.id+" | Slot # "+node+" Goes into Slot # "+slot);
            con.query("UPDATE `usernodes1` SET `n"+slot+"` = ? WHERE `id` = ?", [node,item.id]);
          }
        }

        async function processArray(array) {
          for (const item of array) {
            await delayedLog(item);
          }
          console.log("**#1 DONE** "+ functions.dateTime());
        }

        try {
          con.query("SELECT id,n1,n2,n3,n4,n5,n6,n7,n8,n9,n10 FROM usernodes2", function (err, result) {
            if(err){console.log(err);}
            //console.log(result);
            let parsed = JSON.stringify(result);
            let obj = JSON.parse(parsed);
            processArray(obj);
          })
        } catch (e) {
          console.log(e)
        }
        con.releaseConnection(connection);

});
